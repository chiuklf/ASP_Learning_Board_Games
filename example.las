cell(0..3,0..1).

empty(0,cell(1..3,0..1)).
red(0,cell(0,0..1)).


%validMove(S,cell(X1,Y1),cell(X2,Y2)) :- empty(S,cell(X2,Y2)), red(S,cell(X1,Y1)), X2=X1+1, Y1=Y2.

0 {move(S,cell(X1,Y1),cell(X2,Y2))} 1 :- validMove(S,cell(X1,Y1),cell(X2,Y2)).


empty(S+1,C) :- move(S,C,_), not finished(S), S<30.
empty(S+1,C) :- empty(S,C), not move(S,C,_), not move(S,_,C), not finished(S), S<30.

red(S+1,C) :- move(S,_,C), not finished(S), S<30.
red(S+1,C) :- red(S,C), not move(S,C,_), not move(S,_,C), not finished(S), S<30.

%finished(S) :- red(S,cell(3,0)).
%			red(S,cell(3,1)),
%			red(S,cell(3,2)),
%			red(S,cell(3,3)).

:- move(S,cell(X1,Y1),cell(X2,Y2)), move(S,cell(X3,Y3),cell(X4,Y4)), X1!=X3.
:- move(S,cell(X1,Y1),cell(X2,Y2)), move(S,cell(X3,Y3),cell(X4,Y4)), Y1!=Y3.
:- move(S,cell(X1,Y1),cell(X2,Y2)), move(S,cell(X3,Y3),cell(X4,Y4)), X2!=X4.
:- move(S,cell(X1,Y1),cell(X2,Y2)), move(S,cell(X3,Y3),cell(X4,Y4)), Y2!=Y4.
%:- not finished(_).


#maxv(5).
#max_penalty(7).

#modeh(validMove(var(t),cell(var(X1),var(Y1)),cell(var(X2),var(Y2)))).
#modeb(1,empty(var(t),cell(var(X2),var(Y2)))).
#modeb(1,red(var(t),cell(var(X1),var(Y1)))).
#modeb(1,var(X2) = var(X1) + const(c)).
#modeb(1,var(Y2) = var(Y1)).

#constant(c, 1).

#pos(p0, {validMove(0,cell(0,0),cell(1,0)),validMove(0,cell(0,1),cell(1,1))}, {validMove(0,cell(0,0),cell(2,0)),validMove(0,cell(0,0),cell(1,1))}, {empty(0,cell(1,0)).empty(0,cell(2,0)).empty(0,cell(1,1)).empty(0,cell(2,1)).empty(0,cell(3,1)).red(0,cell(0,0)).red(0,cell(0,1)).empty(1,cell(2,0)).empty(1,cell(3,0)).empty(1,cell(1,1)).empty(1,cell(2,1)).empty(1,cell(3,1)).move(0,cell(0,0),cell(1,0)).red(1,cell(1,0)).empty(1,cell(0,0)).move(1,cell(1,0),cell(2,0)).}).

