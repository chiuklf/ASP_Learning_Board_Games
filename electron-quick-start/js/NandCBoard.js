var type = "WebGL"
if(!PIXI.utils.isWebGLSupported()){
  type = "canvas"
}

//Create the renderer
//Aliases
var Container = PIXI.Container,
    autoDetectRenderer = PIXI.autoDetectRenderer,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    TextureCache = PIXI.utils.TextureCache,
    Texture = PIXI.Texture,
    Sprite = PIXI.Sprite;
    crossOffsetx = 80;
    crossOffsety = 120;
    noughtOffsetx = 85;
    noughtOffsety = 130;
    boardXCoord = 50;
    boardYCoord = 40;
    turn = 0;
    test = new PIXI.interaction.InteractionData();
    coordinates = [{x:145,y:178, cell:4, sprite:null, state:EMPTY}, {x:295,y:178, cell:3, sprite:null, state:EMPTY}, {x:445,y:178, cell:8, sprite:null, state:EMPTY}, 
                   {x:145,y:328, cell:9, sprite:null, state:EMPTY}, {x:295,y:328, cell:5, sprite:null, state:EMPTY}, {x:445,y:328, cell:1, sprite:null, state:EMPTY}, 
                   {x:145,y:478, cell:2, sprite:null, state:EMPTY}, {x:295,y:478, cell:7, sprite:null, state:EMPTY}, {x:445,y:478, cell:6, sprite:null, state:EMPTY}];
var app, boardTexture, crosses, noughts, board;

function init() {
  app = new PIXI.Application(550, 550, {view:document.getElementById('nandc-game-canvas')});
  document.getElementById('nandc-canvas').appendChild(app.view);

  boardTexture = Texture.fromImage('./images/NandCBoard.jpg');
  crosses = Texture.fromImage('./images/x.png');
  noughts = Texture.fromImage('./images/o.png');
  // Scale mode for pixelation
  crosses.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
  noughts.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;

  board = new PIXI.Sprite(boardTexture);
  board.position.set(boardXCoord,boardYCoord);
  board.interactive = true;
  board.buttonMode = true;
  board.on('pointerdown', onClick);
  board.scale.set(0.75)

  app.stage.addChild(board);

  // for (var i=0; i<9; i++) {
  //   var c = coordinates[i].cell
  //   positions.push({cell:c, state:EMPTY, sprite:null});
  //   originPosition.push({cell:c, state:EMPTY, sprite:null});
  // }

  var lineReader = require('readline').createInterface({
    input: require('fs').createReadStream('test.las')
  });

  lineReader.on('line', function (line) {
    if (line.substring(0,5) == "#mode") {
      var index = line.indexOf(").");
      if (index == -1) {return;}
      new_mode("rule", line.substring(0,index+1));
      new_mode("strat", line.substring(0,index+1));
      console.log('Line from file:', line.substring(0,index+1));
    }

  });
}

function onClick(mData) {
  console.log("SDKLFJSLDKJ");
  var point = findCoord(mData.data.originalEvent.pageX, mData.data.originalEvent.pageY);
  if (point == null) {return;}
  var x = point[0];
  var y = point[1];
  // console.log("This X: " + x);
  // console.log("This Y: " + y);
  // console.log("This 2: " + point[2]);

  var index = findIndex(point[2],coordinates);

  if (!playToggle) {
    if (!exclusionToggle && holdToggle && recording) {
      $(".alert").show();
      $(".alert").fadeTo(1000, 200).slideUp(200, function(){
        $(".alert").slideUp(200);
      });
      if (turn%2==0) {
        inclusionMoves.push({cell:point[2], turn: turn, state:CROSSES, pred:predicate});
      } else {
        inclusionMoves.push({cell:point[2], turn: turn, state:NOUGHTS, pred:predicate});
      }
    } else
    if (!exclusionToggle) {
      if (turn%2 == 0) {
        cross = new PIXI.Sprite(crosses);
        cross.position.set(x-crossOffsetx,y-crossOffsety);
        cross.scale.set(0.2);
        app.stage.addChild(cross);

        positions.push({cell:point[2], state:CROSSES, sprite:cross});

        // positions[index].sprite = cross;
        // positions[index].state = CROSSES;
        // coordinates[index].sprite = cross;

        var i = findIndex(point[2],coordinates);
        coordinates[i].state = CROSSES;

        if (recording) {
          moves.push({cell:point[2], pred:predicate, state:CROSSES});
        } else {
          movesBefore.push({cell:point[2], pred:predicate, state:CROSSES}); 
        }
        turn++; 
        //state += updateState(point[2], turn, CROSSES);
      } else {
        nought = new PIXI.Sprite(noughts);
        nought.position.set(x-noughtOffsetx,y-noughtOffsety);
        nought.scale.set(0.13);
        app.stage.addChild(nought);

        positions.push({cell:point[2], state:NOUGHTS, sprite:nought});

        var i = findIndex(point[2],coordinates);
        coordinates[i].state = NOUGHTS;
        // positions[index].sprite = nought;
        // positions[index].state = NOUGHTS;
        // coordinates[index].sprite = nought;

        if (recording) {
          moves.push({cell:point[2], pred:predicate, state:NOUGHTS});
        } else {
          movesBefore.push({cell:point[2], pred:predicate, state:NOUGHTS});          
        }
        turn++; 
        //state += updateState(point[2], turn, NOUGHTS);
      }
    } else {
      $(".alert").show();
      $(".alert").fadeTo(1000, 200).slideUp(200, function(){
        $(".alert").slideUp(200);
      });
      if (exclusionMoveCount%2==0) {
        exclusionMoves.push({cell:point[2], turn: exclusionMoveCount, state:CROSSES, pred:predicate});
      } else {
        exclusionMoves.push({cell:point[2], turn: exclusionMoveCount, state:NOUGHTS, pred:predicate});
      }
    }
  } else {
    if (turn%2==0) {
      cross = new PIXI.Sprite(crosses);
      cross.position.set(x-crossOffsetx,y-crossOffsety);
      cross.scale.set(0.2);
      app.stage.addChild(cross);

      coordinates[index].sprite = cross;

      if (turn==8) {return;}

      var substr = "\nmove(" + turn + ",cell(" + point[2] + "),c).\n";
      console.log("SUBSTRIG: " + substr);
      var fs = require('fs');
      fs.appendFile("./OnePlayer.lp", substr, function(err) {
        console.log("<44>");
        if(err) return console.log(err);
        turn++; 

        var exec = require('child_process').exec;
        var cmd = '..\\clingo OnePlayer.lp';

        exec(cmd, function(error, stdout, stderr) {
          console.log("<2>");

          var outputArray = stdout.split('\n');
          var lastAnswerIndex = -1;

          for (var i=outputArray.length; i>=0; i--) {
            if (outputArray[i] != null && outputArray[i].substring(0,6) == "Answer") {
              lastAnswerIndex = i+1;
              break;
            }
          }

          outputArray = outputArray[lastAnswerIndex].split(' ');
          var aiMove = outputArray[outputArray.length-1];

          var cellOutput =  aiMove.charAt(12);
          console.log("THISS??: " + cellOutput);

          var c = parseInt(cellOutput);
          console.log("C: " + c);

          var coordinateIndex = findIndex(c, coordinates);
          
          nought = new PIXI.Sprite(noughts);

          nought.position.set(coordinates[coordinateIndex].x - noughtOffsetx,coordinates[coordinateIndex].y - noughtOffsety);
          nought.scale.set(0.13);
          app.stage.addChild(nought);

          coordinates[coordinateIndex].sprite = nought;

          var s = "move(" + turn + ",cell(" + cellOutput + "),n).\n";
          fs.appendFile("./OnePlayer.lp", s, function(err) {
            if(err) return console.log(err);

            turn++;
            console.log("<3>");

            fs.readFile('./OnePlayer.lp', function read(err, data) {
              if (err) {throw err;}
              console.log("<222>");
              var lines = data.toString().split('\n');

              for (var i=0; i<lines.length; i++) {

                if (lines[i].substring(0,6) == "#const") {

                  lines.splice(i,1);
                  break;
                }
              }
              lines = lines.join('\n');
              lines += "\n#const max=" + (turn+2) + ".\n";
              fs.writeFile('./OnePlayer.lp', lines, function(err) {
                if(err) return console.log(err);
              });
            });

          }); 

        });



      }); 



 
    }
  }
  
}

function findCoord(x, y) {
  for (var i=0; i<9; i++) {
    if (Math.abs(coordinates[i].x - x) < 50 && Math.abs(coordinates[i].y - y) < 50) {
      console.log("HERERERER");
      return [coordinates[i].x, coordinates[i].y, coordinates[i].cell];
    }
  }
  return null;
}

function changeGameState(toGameState) {
  for (var i=0; i<positions.length; i++) {
    app.stage.removeChild(positions[i].sprite);
  }
  for (var i=0; i<toGameState.length; i++) {
    app.stage.addChild(toGameState[i].sprite);
  }
}


function findIndex(cell, array) {
  for (var i=0; i<array.length; i++) {
    if (array[i].cell == cell) {
      return i;
    }
  }
  return null;
}

function resetGame() {

  var fs = require('fs');
  var content = fs.readFileSync("./OnePlayer.lp", 'utf8');
  console.log("<6>");
  var text_array = content.toString().split("\n");
  var newText = text_array.slice(0,text_array.length-turn-1).join("\n");

  fs.writeFileSync("./OnePlayer.lp", newText, 'utf8');

  content = fs.readFileSync("./OnePlayer.lp", 'utf8');
  console.log("<7>");
  var saveTurn = turn-1;
  console.log("TURN HERE: " + saveTurn);
  var substr = '#const t=' + saveTurn + ".";
  var replace = content.replace(substr, "#const t=-1.\n");
  fs.writeFileSync("./OnePlayer.lp", replace, 'utf8');
  console.log("<8>");
  reset();


}