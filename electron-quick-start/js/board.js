var type = "WebGL"
if(!PIXI.utils.isWebGLSupported()){
  type = "canvas"
}

//Create the renderer
//Aliases
var Container = PIXI.Container,
    autoDetectRenderer = PIXI.autoDetectRenderer,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    TextureCache = PIXI.utils.TextureCache,
    Texture = PIXI.Texture,
    Sprite = PIXI.Sprite;
    offset = 70;
    xOrigin = 59;
    yOrigin = 45;
    boardXCoord = 50;
    boardYCoord = 40;


var app, boardTexture, rebels, officers, board;
function init() {
  app = new PIXI.Application(700, 630, {view:document.getElementById('game-canvas')});
  document.getElementById('canvas').appendChild(app.view);

  boardTexture = Texture.fromImage('./images/board.png');
  rebels = Texture.fromImage('./images/Rebels.png');
  officers = Texture.fromImage('./images/Officers.png');
  // Scale mode for pixelation
  rebels.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
  officers.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;

  board = new PIXI.Sprite(boardTexture);
  board.position.set(boardXCoord,boardYCoord);
  app.stage.addChild(board);

  for (var i=0; i<7; i++) {
    for (var j=0; j<7; j++) {
      if (i>1 && i<5 && j<3) {
        var x = boardXCoord + xOrigin + (i*offset);
        var y = boardYCoord + yOrigin + (j*offset);
        if ((j == 0 && i ==2) || (j==0 && i==4)) {
          createOfficer(i,j);
        } else {
          positions.push({x:x, y:y, i:j, j:i, state:EMPTY, sprite:null});
        }
        continue;
      }
      if ((i<2 && j>4) || (i>4 && j>4) || j<2) {
        continue;
      }
      createRebels(i,j);
    }
  }
  for (var i=0; i<positions.length; i++) {
    originPosition.push({x:positions[i].x, y:positions[i].y, i:positions[i].i, j:positions[i].j, sprite:positions[i].sprite, state:positions[i].state});
  }

  var lineReader = require('readline').createInterface({
    input: require('fs').createReadStream('test.las')
  });

  lineReader.on('line', function (line) {
    if (line.substring(0,5) == "#mode") {
      var index = line.indexOf(").");
      if (index == -1) {return;}
      new_mode("rule", line.substring(0,index+1));
      new_mode("strat", line.substring(0,index+1));
      console.log('Line from file:', line.substring(0,index+1));
    }

  });
}

function createOfficer(i, j) {
  // var officer = new Sprite(resources["./Images/Officers.png"].texture);
  var officer = new Sprite(officers);
  var x = boardXCoord + xOrigin + (i*offset);
  var y = boardYCoord + yOrigin + (j*offset);
  positions.push({x:x, y:y, i:j, j:i, state:OFFICER, sprite:officer});
  officer.position.set(x,y);
  officer.scale.set(0.075);

  officer.interactive = true;
  officer.buttonMode = true;
  officer.anchor.set(0.5);
  officer
    .on('pointerdown', onDragStart)
    .on('pointerup', onDragEnd)
    .on('pointerupoutside', onDragEnd)
    .on('pointermove', onDragMove);
  app.stage.addChild(officer);
}

function createRebels(i, j) {

  // var rebel = new Sprite(resources["./Images/Rebels.png"].texture);
  var rebel = new Sprite(rebels);
  var x = boardXCoord + xOrigin + (i*offset);
  var y = boardYCoord + yOrigin + (j*offset);
  positions.push({x:x, y:y, i:j, j:i, state:REBEL, sprite:rebel});
  rebel.position.set(x,y);
  rebel.scale.set(0.075);

  rebel.interactive = true;
  rebel.buttonMode = true;
  rebel.anchor.set(0.5);
  rebel
    .on('pointerdown', onDragStart)
    .on('pointerup', onDragEnd)
    .on('pointerupoutside', onDragEnd)
    .on('pointermove', onDragMove);

  app.stage.addChild(rebel);
}

var dragX, dragY, originIndex;

function onDragStart(event) {
// store a reference to the data
// the reason for this is because of multitouch
// we want to track the movement of this particular touch
  console.log("onDragStart");
  this.data = event.data;
  this.alpha = 0.5;
  this.dragging = true;
  dragX = this.x;
  dragY = this.y;
  originIndex = findIndex(this.x, this.y, positions);
}

function onDragEnd() {
    this.alpha = 1;
    this.dragging = false;
    console.log("onDragEnd");

    // set the interaction data to null
    this.data = null;
    var range = withinRange(this.x, this.y);
    if (!range[0]) {
      this.x = dragX;
      this.y = dragY;
    } else {
      if (!playToggle) {
        if (!exclusionToggle && holdToggle && recording) {
          console.log("<999>");
          $(".alert").show();
          $(".alert").fadeTo(1000, 200).slideUp(200, function(){
            $(".alert").slideUp(200);
          });
          if (turn%2==0) {
            inclusionMoves.push({from:positions[originIndex], to:positions[range[3]], pred:predicate, turn: turn});
          } else {
            inclusionMoves.push({from:positions[originIndex], to:positions[range[3]], pred:predicate, turn: turn});
          }
          this.x = dragX;
          this.y = dragY;

        } else      
        if (!exclusionToggle) {
          console.log("<111>");
          this.x = range[1];
          this.y = range[2];

          var tempstate = positions[originIndex].state;
          var tempsprite = positions[originIndex].sprite;
          positions[originIndex].state = EMPTY;
          positions[originIndex].sprite = null;
          positions[range[3]].state = tempstate;
          positions[range[3]].sprite = tempsprite;

          if (recording) {
            moves.push({from:positions[originIndex], to:positions[range[3]], state:tempstate, pred:predicate});
          } else {
            movesBefore.push({from:positions[originIndex], to:positions[range[3]], state:tempstate, pred:predicate});
          }
          turn++;
          // state += updateState(positions[originIndex], positions[range[3]], turn, tempstate);
        } else {
          console.log("HEREERE");
          $(".alert").show();
          $(".alert").fadeTo(1000, 200).slideUp(200, function(){
            $(".alert").slideUp(200);
          }); 
          exclusionMoves.push({from:positions[originIndex], to:positions[range[3]], pred:predicate, turn: exclusionMoveCount});
          this.x = dragX;
          this.y = dragY;
        }
      } else {

      }
    }
}

function onDragMove() {

    if (this.dragging) {
        var newPosition = this.data.getLocalPosition(this.parent);
        this.x = newPosition.x;
        this.y = newPosition.y;
    }
    // renderer.render(stage);

}

function closest(x, y) {
  var index = 0;
  var newX = positions[0].x;
  var newY = positions[0].y;
  var minX = Math.abs(newX - x);
  var minY = Math.abs(newY - y);
  for (var i=1; i<positions.length; i++) {
    var tempX = Math.abs(positions[i].x - x);
    var tempY = Math.abs(positions[i].y - y);
    if ((minX >= tempX && minY > tempY)
      || (minX > tempX && minY >= tempY)) {
      index = i;
      newX = positions[i].x;
      newY = positions[i].y;
      minX = tempX;
      minY = tempY;
    }
  }
  return [newX, newY, index];
}

function withinRange(x, y) {
  var newCoord = closest(x, y);
  var newX = newCoord[0];
  var newY = newCoord[1];
  var index = newCoord[2];

  if (Math.abs(newX - x) < 15 && Math.abs(newY - y) < 15) {
    return [true, newX, newY, index];
  }
  return false;
}

function findIndex(x, y, array) {
  for (var i=0; i<array.length; i++) {
    if (array[i].x == x && array[i].y == y) {
      return i;
    }
  }
  return null;
}

function changeGameState(gameState) {
  // console.log("GameState: ");
  for (var i=0; i<gameState.length; i++) {
    // console.log("\nx: " + gameState[i].x + " y: " + gameState[i].y + " state: " + gameState[i].sprite);
    if (gameState[i].sprite != null) {
      gameState[i].sprite.position.set(gameState[i].x, gameState[i].y);
    }
  }
}

function resetGame() {

  var fs = require('fs');
  var content = fs.readFileSync("./AsaltoOnePlayer.lp", 'utf8');
  console.log("<6>");
  var text_array = content.toString().split("\n");
  var newText = text_array.slice(0,text_array.length-turn-1).join("\n");

  fs.writeFileSync("./AsaltoOnePlayer.lp", newText, 'utf8');

  content = fs.readFileSync("./AsaltoOnePlayer.lp", 'utf8');
  console.log("<7>");
  var saveTurn = turn-1;
  console.log("TURN HERE: " + saveTurn);
  var substr = '#const t=' + saveTurn + ".";
  var replace = content.replace(substr, "#const t=-1.\n");
  fs.writeFileSync("./AsaltoOnePlayer.lp", replace, 'utf8');

  reset();


}