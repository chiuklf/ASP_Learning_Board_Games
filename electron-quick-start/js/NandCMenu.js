var interpretation = false;
var posEgCount = 0;
var negEgCount = 0;
var exampletype, example;
var recordTurnStart = 0;
state = "";
var counter = 1;
var exclusion = "";
var inclusion = ",";

function enter_mode (type) {
	var text = $('#' + type + '_enter_mode').val();
	new_mode(type,text);
};

function new_mode(type,text) {
	console.log("KLSDJFLKSDJF: " + text);
	var list = '#' + type + '_delete' + counter;
	console.log("LIST: " + list);
	$('#' + type + '_mode_list').append('<li><span>'+ text + '</span><input type="submit" id="'
		+ type + '_edit' 
		+ counter + '" value="Edit">' + '<input type="submit" class="done" id="' 
		+ type + '_delete' 
		+ counter + '" value="Delete">' +'</li>');

	$('#' + type + '_edit' + counter).click(function(){
	    $(this).prev().attr('contenteditable','true');
	    $(this).prev().focus();
	});

	$('#' + type + '_delete' + counter).click(function(){
		console.log("CALLED");
	    $(this).parent().remove();
	});

 	counter++;
}

function enter_valid_moves(type) {
	var text = $('#enter_' + type + '_move').val();
	$('#' + type + '_selectable').append('<li class="ui-widget-content">' + text + '</li>');
}

$( function() {
  $( ".selectable" ).selectable({
  	tolerance: "fit",
  });

  $( "#strat_selectable" ).on("selectableselected", function( event, ui ) {
	$(".ui-selected", this).each(function () {
        var index = $("#strat_selectable li").index(this);
        console.log("THIS: " + $(this).text());
        predicate = $(this).text();
	});
  });

  $( "#rule_selectable" ).on("selectableselected", function( event, ui ) {
	$(".ui-selected", this).each(function () {
        var index = $("#rule_selectable li").index(this);
        console.log("THIS: " + $(this).text());
        predicate = $(this).text();
	});
  });
});

function addPanel(type) {
	var order;
	if (type == "cautious_ordering") {
		order = cOrdering_count;
		cOrdering_count++;
	} else {
		order = bOrdering_count;
		bOrdering_count++;
	}
	$('#' + type).append('<li><div class="panel panel-primary">' +
                          '<div class="panel-body">' +
                          '<ul id="' + type + '_count' + order + '" class="sortable-list ui-sortable" name="in_available_fields">' +
                          '</ul></div></div></li>');
	$( function() {
	    $('.sortable-list').sortable({
	      connectWith: '.sortable-list',
	      placeholder: 'placeholder'
	    });
  	});
}

function addOrderings() {

	// $('#examples li').each(function(i) {
	// 	console.log("THIS: " + $(this).text());
	// })
	var str = "";
	console.log("COUNT: " + bOrdering_count);
	for (var i=0; i<bOrdering_count; i++) {
		var id = "#brave_ordering_count" + i + " li";
		var count = 0;
		// console.log("ID: " + id);
		$(id).each(function(i) {
			var txt = $(this).text();
			// console.log("THIS: " + $(this).text());
			// console.log("I: " + $(this).index());
			$(id).each(function(i) {
				if ($(this).index() <= count) return;
				str += "#brave_ordering(" + txt + ", "
				str += $(this).text() + ").\n"
				// console.log("AND: " + $(this).text());
			});
			count++;
		});
	}
	str += "\n";

	for (var i=0; i<cOrdering_count; i++) {
		var id = "#cautious_ordering_count" + i + " li";
		var count = 0;
		$(id).each(function(i) {
			var txt = $(this).text();
			$(id).each(function(i) {
				if ($(this).index() <= count) return;
				str += "#cautious_ordering(" + txt + ", "
				str += $(this).text() + ").\n"
			});
			count++;
		});
	}
	str += "\n";
	// console.log("STRING: " + str);
	var fs = require('fs');
	fs.appendFile("./test.las", str, function(err) {
		if(err) return console.log(err);

		console.log("The file was saved!");
	}); 
}

async function record(type) {
	if(document.getElementById(type + "_start").value=="Record") {
		if (predicate == null) {
   			alert("Please pick a predicate");
   			return;
   		}
		recording = true;
		recordTurnStart = turn;
		exampletype = document.getElementById(type + "_exampleswitch").checked;

   		document.getElementById(type + "_start").value="Stop";
   		for (var i=0; i<positions.length; i++) {
   			recordPosition.push({cell:positions[i].cell, state:positions[i].state, sprite:positions[i].sprite});
   		}
   	} else {

		if (exampletype && type != 'strat') {
			example = "#pos(p" + posEgCount + ", {";
			posEgCount++;
		} else if (!exampletype && type != 'strat') {
			example = "#neg(n" + negEgCount + ", {";
			negEgCount++;
		} else if (exampletype && type == 'strat') {
			example = "#pos(" + document.getElementById("example_name").value + ", {";
		} else {
			example = "#neg(" + document.getElementById("example_name").value + ", {";
		}


		var cell;
		console.log("FINISHED MOVE: " + moves);
		for (var i=0; i<moves.length; i++) {
			cell = moves[i].cell;
			if (moves[i].state == NOUGHTS) {
				var t = recordTurnStart+i
				example += moves[i].pred + "(" + t + ",cell(" + cell 
						+ "),n),";

			}
			if (moves[i].state == CROSSES) {
				var t = recordTurnStart+i
				example += moves[i].pred + "(" + t + ",cell(" + cell
						+ "),c),";
			}
			console.log("Example: " + example);

		}

		for (var i=0; i<exclusionMoves.length; i++) {
			cell = exclusionMoves[i].cell;
			var t = recordTurnStart + exclusionMoves[i].turn;
			var s = exclusionMoves[i].state;
			var str = "";
			if (s == NOUGHTS) {
				str = "n";
			} else {
				str = "c";
			}
			exclusion += exclusionMoves[i].pred + "(" + t + ",cell(" + cell + ")," + str + "),";
			console.log("Exclusion Example: " + exclusion);
		}

		for (var i=0; i<inclusionMoves.length; i++) {
			cell = inclusionMoves[i].cell;
			var t = inclusionMoves[i].turn;
			var s = inclusionMoves[i].state;
			var str = "";
			if (s == NOUGHTS) {
				str = "n";
			} else {
				str = "c";
			}
			inclusion += inclusionMoves[i].pred + "(" + t + ",cell(" + cell + ")," + str + "),";
			console.log("Exclusion Example: " + inclusion);
		}


		example = example.substring(0, example.length-1);
		inclusion = inclusion.substring(0, inclusion.length-1);		
		exclusion = exclusion.substring(0, exclusion.length-1);

		var fs = require('fs');

		fs.readFile('NandC.lp', function read(err, data) {
			if (err) {throw err;}
			console.log("<11>");
			var lines = data.toString().split('\n');

			for (var i=0; i<lines.length; i++) {
				console.log("LINE: " + lines[i]);
				if (lines[i].substring(0,5) == "#show") {
					lines.splice(i,1);
					i--;
				}
				if (lines[i].substring(0,4) == "move") {
					lines.splice(i,1);
					i--;
				}
			}

			lines = lines.join('\n');
			var lis = [];
			if (type != 'strat') {
				lis = document.getElementById("show_selectable").getElementsByTagName("li");
			} else {
				lis = document.getElementById("showStrat_selectable").getElementsByTagName("li");
			}

			var substr = "";

			for (var i=0; i<movesBefore.length; i++) {
				var cl = movesBefore[i].cell;
				var s = movesBefore[i].state;
				if (s == CROSSES) {
					substr += "\nmove(" + i + ",cell(" + cl + "),c).";
				} else {
					substr += "\nmove(" + i + ",cell(" + cl + "),n)."; 
				}
			}

			for (var i=0; i<moves.length; i++) {
				var cl = moves[i].cell;
				var s = moves[i].state;
				var j = turn - movesBefore.length + i + 1; 
				if (s == CROSSES) {
					substr += "\nmove(" + j + ",cell(" + cl + "),c).";
				} else {
					substr += "\nmove(" + j + ",cell(" + cl + "),n)."; 
				}
			}

			for (var i=0; i<lis.length; i++) {
				substr += "\n#show " + lis[i].innerText + ".";
			}



			lines += substr;
			
			fs.writeFile('NandC.lp', lines, function(err) {
	          	if(err) return console.log(err);
	          	console.log("<2>");

				var exec = require('child_process').exec;
		      	var cmd = '..\\clingo NandC.lp';

			    exec(cmd, function(error, stdout, stderr) {
			        console.log("<1>: " + stdout);


					var fs = require('fs');


				        var out = stdout.split('\n');
				        var as = out[4];

				        var predicates = as.split(' ');

				       	for (var i=0; i<predicates.length; i++) {
				       		state += predicates[i] + ".";
				       	}

				       	// state = state.substring(0, state.length-1);

						example += inclusion + "}, {" + exclusion + "}, {" + state + "}).\n";

						fs.appendFile("./test.las", example, function(err) {
						    if(err) return console.log(err);

						    console.log("The file was saved!");
						   	console.log("<5>");

						   	reset();
						}); 
		 

			   		if (type == "strat") {
			   			var name = document.getElementById("example_name").value;
			   			$('#examples').append('<li class="eg">' + name + '</li>');
			   		}
			        console.log("<6>");

			    });


			});



		});



   	}

}


function generateState(turn, i) {
	switch(coordinates[i].state) {
		case EMPTY:
			var state = "empty(" + turn + ",cell(" + coordinates[i].cell + ")).";
			return state;
		case CROSSES:
			var state = "crosses(" + turn + ",cell(" + coordinates[i].cell + ")).";
			return state;
		case NOUGHTS:
			var state = "noughts(" + turn + ",cell(" + coordinates[i].cell + ")).";
			return state;
		default:
			console.log("Not suppose to reach here in generateState");
			return null;
	}
}

function initState() {
	var state = "";
	for (var i=0; i<coordinates.length; i++) {
		state += generateState(0, i);
	}
	return state;
}

// function updateState(cell, turn, player) {
// 	console.log("CALLED updateState");
// 	var state = "";
// 	for (var i=0; i<coordinates.length; i++) {
// 		if (coordinates[i].cell == cell && player == NOUGHTS) { 
// 			state += "noughts(" + turn + ",cell(" + cell + ")).";
// 			continue;
// 		}
// 		if (coordinates[i].cell == cell  && player == CROSSES) { 
// 			state += "crosses(" + turn + ",cell(" + cell + "))."; 
// 			continue;
// 		}
// 		state += generateState(turn, i);
// 	}
// 	return state;
// }

function showControl(name, control) {
	var checkbox = document.getElementById(name);
	if (!checkbox.checked) {
		document.getElementById(control).style.display = "block";
		exclusionToggle = true;
	} else {
		document.getElementById(control).style.display = "none";
		exclusionToggle = false;
	}
}

function prev_move(name) {
	if (exclusionMoveCount == 0) {
		return;
	}
	exclusionMoveCount--;
	document.getElementById(name).innerHTML = "Turn " + exclusionMoveCount;

	var index = recordPosition.length + exclusionMoveCount;
	app.stage.removeChild(positions[index].sprite);
}

function next_move(name) {
	if (exclusionMoveCount == moves.length) {
		return;
	}

	var index = recordPosition.length + exclusionMoveCount;
	app.stage.addChild(positions[index].sprite);

	exclusionMoveCount++;
	document.getElementById(name).innerHTML = "Turn " + exclusionMoveCount;
}


function reset() {
	for (var i=0; i<coordinates.length; i++) {
		console.log("Coordinates: " + coordinates[i].sprite);
	}
	for (var i=0; i<positions.length; i++) {
		app.stage.removeChild(positions[i].sprite);
	}
	positions = new Array();
	for (var i=0; i<coordinates.length; i++) {
		coordinates[i].sprite = null;
	}
   	// changeGameState(originPosition);

    exclusionMoves = [];
    inclusionMoves = [];
    moves = [];
    recordPosition = [];
	movesBefore = [];
    example = "";
    exclusionMoveCount = 0;

	state = "";
	recordTurnStart = 0;
	exclusionMoveCount = 0;
   	predicate = null;
   	exclusionToggle = false;
   	holdToggle = false;
   	recording = false;

   	turn = 0;
   	document.getElementById("rule_start").value="Record";
   	document.getElementById("strat_start").value="Record";
   	document.getElementById("inclusionswitch2").checked = true;
   	document.getElementById("inclusionswitch").checked = true;
   	document.getElementById("holdRule").checked = false;
   	document.getElementById("holdStrat").checked = false;
	document.getElementById("strat_move_control").style.display = "none";
	document.getElementById("move_control").style.display = "none";
	document.getElementById("move_count").innerHTML = "Turn " + exclusionMoveCount;
	document.getElementById("strat_move_count").innerHTML = "Turn " + exclusionMoveCount;
}

