cell(0..1,2..4).
cell(2..4,0..6).
cell(5..6,2..4).

fort(cell(0..2,2..4)).
diagonal(cell(0,2)).
diagonal(cell(0,4)).
diagonal(cell(1,3)).
diagonal(cell(2,0)).
diagonal(cell(2,2)).
diagonal(cell(2,4)).
diagonal(cell(2,6)).
diagonal(cell(3,1)).
diagonal(cell(3,3)).
diagonal(cell(3,5)).
diagonal(cell(4,0)).
diagonal(cell(4,2)).
diagonal(cell(4,4)).
diagonal(cell(4,6)).
diagonal(cell(5,3)).
diagonal(cell(6,2)).
diagonal(cell(6,4)).

rebel(0,cell(2,0..1)).
rebel(0,cell(2,5..6)).
rebel(0,cell(3..4,0..6)).
rebel(0,cell(5..6,2..4)).

officer(0,cell(1,2)).
officer(0,cell(1,4)).

empty(0,cell(0,2..4)).
empty(0,cell(1,3)).
empty(0,cell(2,2..4)).

turn(0,r).

even(cell(X,Y)) :- cell(X,Y), (X+Y)\2==0.

diagonal(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1-X2=1, Y1-Y2=1.
diagonal(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1-X2=-1, Y1-Y2=1.
diagonal(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1-X2=1, Y1-Y2=-1.
diagonal(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1-X2=-1, Y1-Y2=-1.

adjacent(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1-X2=1, Y1=Y2.
adjacent(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1-X2=-1, Y1=Y2.
adjacent(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1=X2, Y1-Y2=1.
adjacent(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1=X2, Y1-Y2=-1.

withinRange(C1,C2) :- diagonal(C1,C2), even(C1). 
withinRange(C1,C2) :- adjacent(C1,C2). 

connected(cell(X1,Y1),C2,cell(X3,Y3)) :- diagonal(cell(X1,Y1),C2), even(cell(X1,Y1)), diagonal(C2,cell(X3,Y3)), X1-X3=2, Y1-Y3=2.
connected(cell(X1,Y1),C2,cell(X3,Y3)) :- diagonal(cell(X1,Y1),C2), even(cell(X1,Y1)), diagonal(C2,cell(X3,Y3)), X1-X3=-2, Y1-Y3=2.
connected(cell(X1,Y1),C2,cell(X3,Y3)) :- diagonal(cell(X1,Y1),C2), even(cell(X1,Y1)), diagonal(C2,cell(X3,Y3)), X1-X3=2, Y1-Y3=-2.
connected(cell(X1,Y1),C2,cell(X3,Y3)) :- diagonal(cell(X1,Y1),C2), even(cell(X1,Y1)), diagonal(C2,cell(X3,Y3)), X1-X3=-2, Y1-Y3=-2.

connected(cell(X1,Y1),C2,cell(X3,Y3)) :- adjacent(cell(X1,Y1),C2), adjacent(C2,cell(X3,Y3)), X1-X3=2, Y1=Y3.
connected(cell(X1,Y1),C2,cell(X3,Y3)) :- adjacent(cell(X1,Y1),C2), adjacent(C2,cell(X3,Y3)), X1-X3=-2, Y1=Y3.
connected(cell(X1,Y1),C2,cell(X3,Y3)) :- adjacent(cell(X1,Y1),C2), adjacent(C2,cell(X3,Y3)), X1=X3, Y1-Y3=2.
connected(cell(X1,Y1),C2,cell(X3,Y3)) :- adjacent(cell(X1,Y1),C2), adjacent(C2,cell(X3,Y3)), X1=X3, Y1-Y3=-2.

connectedRebel(S,C1,C3) :- connected(C1,C2,C3), rebel(S,C2).

toFort(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X2<=X1.

#modeh(validRebelMove(var(state), var(cell), var(cell))).
#modeh(validOfficerMove(var(state), var(cell), var(cell))).
#modeh(validLeapMove(var(state), var(cell), var(cell))).

#modeb(1,turn(var(state),const(player)), (positive)).
#modeb(1,empty(var(state),var(cell)), (positive)).
#modeb(1,rebel(var(state),var(cell)), (positive)).
#modeb(1,withinRange(var(cell),var(cell)), (positive)).
#modeb(1,fort(var(cell)), (positive)).
#modeb(1,toFort(var(cell),var(cell)), (positive)).
#modeb(1,officer(var(state),var(cell)), (positive)).
#modeb(1,connectedRebel(var(state),var(cell),var(cell)), (positive)).

#constant(player, r).
#constant(player, o).

#maxv(3).
#max_penalty(25).


%validRebelMove Example: Shared body
#pos(p0, {
	validRebelMove(0,cell(2,5),cell(2,4))
}, {
	validRebelMove(1,cell(2,5),cell(2,4)),validRebelMove(0,cell(2,6),cell(2,4)),validRebelMove(0,cell(2,6),cell(2,5)), validRebelMove(0,cell(2,5),cell(2,3)),validRebelMove(0,cell(1,2),cell(0,2)), validRebelMove(0,cell(2,3),cell(2,4))
}, {
	turn(1,o).turn(2,r).rebel(1,cell(2,5)).
	empty(1,cell(2,4)).rebel(1,cell(2,6)).rebel(2,cell(2,5)).rebel(2,cell(2,6)).
}).

%validRebelMove Example: toFort
#pos(p1, {
	validRebelMove(0,cell(3,1),cell(2,2)),validRebelMove(2,cell(3,0),cell(3,1))
},{
	validRebelMove(1,cell(3,0),cell(3,1)),validRebelMove(2,cell(2,1),cell(3,1))
},{
	turn(1,o).turn(2,r).rebel(1,cell(2,2)).rebel(1,cell(2,1)).rebel(1,cell(3,0)).empty(1,cell(3,1)).rebel(2,cell(2,1)).rebel(2,cell(2,2)).rebel(2,cell(3,0)).empty(2,cell(3,1)).
}).

%validRebelMove Example: fort
#pos(p2, {
	validRebelMove(4,cell(1,3),cell(2,3))
}, {
	validRebelMove(5,cell(1,3),cell(2,3)),validRebelMove(6,cell(1,3),cell(2,2)),validRebelMove(8,cell(0,3),cell(2,3)),validRebelMove(8,cell(2,2),cell(0,2))
}, {
	turn(1,o).turn(2,r).turn(3,o).turn(4,r).turn(5,0).turn(6,r).turn(7,o).turn(8,r).rebel(4,cell(1,3)).empty(4,cell(2,3)).rebel(5,cell(1,3)).rebel(5,cell(2,2)).rebel(6,cell(1,3)).rebel(6,cell(2,2)).rebel(8,cell(0,3)).rebel(8,cell(2,2)).empty(8,cell(2,3)).empty(8,cell(0,2)).
}).

%validOfficerMove
#pos(p3, {
	validOfficerMove(1,cell(1,4),cell(1,3)), validOfficerMove(1,cell(1,4),cell(2,4)),
	validOfficerMove(1,cell(1,4),cell(0,4))
}, {
	validOfficerMove(1,cell(2,5),cell(2,4)), validOfficerMove(1,cell(1,4),cell(0,2)),validOfficerMove(1,cell(1,2),cell(2,2)),validOfficerMove(1,cell(3,3),cell(2,3)),validOfficerMove(1,cell(2,3),cell(1,3)), validOfficerMove(1,cell(2,2),cell(2,3))
}, {
	turn(1,o).turn(2,r).rebel(1,cell(2,5)).rebel(1,cell(2,2)).rebel(1,cell(3,3)).officer(1,cell(1,4)).officer(1,cell(1,2)).empty(1,cell(1,3)).empty(1,cell(2,3)).empty(1,cell(0,2)).rebel(2,cell(2,2)).empty(2,cell(2,1)).empty(1,cell(2,4)). empty(1,cell(0,4)).
}).

%validOfficerMove: Officer Moving Down outside fort
#pos(p4, {
	validOfficerMove(3,cell(2,2),cell(3,3))
}, {
	validOfficerMove(0,cell(1,2),cell(2,2))
}, {
	turn(1,o).turn(2,r).turn(3,o).officer(1,cell(1,2)).rebel(1,cell(2,3)).empty(1,cell(3,3)).officer(2,cell(2,2)).empty(2,cell(3,3)).officer(3,cell(2,2)).empty(3,cell(3,3)).
}).

#pos(p5, {
	validLeapMove(3,cell(0,2),cell(2,2))
},{
	validLeapMove(1,cell(2,3),cell(2,1)),validLeapMove(3,cell(0,2),cell(0,3)),validLeapMove(3,cell(1,2),cell(2,2)),validLeapMove(4,cell(0,2),cell(2,2)),validLeapMove(5,cell(0,2),cell(2,2))
},{
	turn(1,o).turn(2,r).turn(3,o).turn(4,r).turn(5,o).rebel(1,cell(2,2)).empty(1,cell(2,3)).empty(1,cell(2,1)).rebel(3,cell(1,2)).officer(3,cell(0,2)).empty(3,cell(2,2)).rebel(4,cell(1,2)).officer(4,cell(0,2)).empty(4,cell(2,2)).rebel(5,cell(1,2)).rebel(5,cell(2,2)).officer(5,cell(0,2)).
}).

#pos(p6, {
	validLeapMove(7,cell(0,2),cell(2,2))
},{
	validLeapMove(1,cell(2,0),cell(2,2)),validLeapMove(3,cell(0,2),cell(2,2))
},{
	turn(1,o).turn(2,r).turn(3,o).turn(4,r).turn(5,o).turn(6,r).turn(7,o).rebel(1,cell(2,0)).rebel(1,cell(2,1)).empty(1,cell(2,2)).officer(3,cell(0,2)).empty(3,cell(1,2)).empty(3,cell(2,2)).rebel(7,cell(1,2)).officer(7,cell(0,2)).empty(7,cell(2,2)).
}).

