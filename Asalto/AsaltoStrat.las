cell(0..1,2..4).
cell(2..4,0..6).
cell(5..6,2..4).

fort(cell(0..2,2..4)).

rebel(0,cell(2,0..1)).
rebel(0,cell(2,5..6)).
rebel(0,cell(3..4,0..6)).
rebel(0,cell(5..6,2..4)).

officer(0,cell(0,2)).
officer(0,cell(0,4)).

empty(0,cell(1,2..4)).
empty(0,cell(0,3)).
empty(0,cell(2,2..4)).

turn(0,r).

even(cell(X,Y)) :- cell(X,Y), (X+Y)\2==0.

diagonal(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1-X2=1, Y1-Y2=1.
diagonal(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1-X2=-1, Y1-Y2=1.
diagonal(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1-X2=1, Y1-Y2=-1.
diagonal(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1-X2=-1, Y1-Y2=-1.

adjacent(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1-X2=1, Y1=Y2.
adjacent(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1-X2=-1, Y1=Y2.
adjacent(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1=X2, Y1-Y2=1.
adjacent(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X1=X2, Y1-Y2=-1.

withinRange(C1,C2) :- diagonal(C1,C2), even(C1). 
withinRange(C1,C2) :- adjacent(C1,C2). 

connected(cell(X1,Y1),C2,cell(X3,Y3)) :- diagonal(cell(X1,Y1),C2), even(cell(X1,Y1)), diagonal(C2,cell(X3,Y3)), X1-X3=2, Y1-Y3=2.
connected(cell(X1,Y1),C2,cell(X3,Y3)) :- diagonal(cell(X1,Y1),C2), even(cell(X1,Y1)), diagonal(C2,cell(X3,Y3)), X1-X3=-2, Y1-Y3=2.
connected(cell(X1,Y1),C2,cell(X3,Y3)) :- diagonal(cell(X1,Y1),C2), even(cell(X1,Y1)), diagonal(C2,cell(X3,Y3)), X1-X3=2, Y1-Y3=-2.
connected(cell(X1,Y1),C2,cell(X3,Y3)) :- diagonal(cell(X1,Y1),C2), even(cell(X1,Y1)), diagonal(C2,cell(X3,Y3)), X1-X3=-2, Y1-Y3=-2.

connected(cell(X1,Y1),C2,cell(X3,Y3)) :- adjacent(cell(X1,Y1),C2), adjacent(C2,cell(X3,Y3)), X1-X3=2, Y1=Y3.
connected(cell(X1,Y1),C2,cell(X3,Y3)) :- adjacent(cell(X1,Y1),C2), adjacent(C2,cell(X3,Y3)), X1-X3=-2, Y1=Y3.
connected(cell(X1,Y1),C2,cell(X3,Y3)) :- adjacent(cell(X1,Y1),C2), adjacent(C2,cell(X3,Y3)), X1=X3, Y1-Y3=2.
connected(cell(X1,Y1),C2,cell(X3,Y3)) :- adjacent(cell(X1,Y1),C2), adjacent(C2,cell(X3,Y3)), X1=X3, Y1-Y3=-2.

validRebelMove(S,cell(X1,Y1),cell(X2,Y2)) :- turn(S,r), empty(S,cell(X2,Y2)), rebel(S,cell(X1,Y1)), withinRange(cell(X1,Y1),cell(X2,Y2)), X2<=X1.
validRebelMove(S,C1,C2) :- turn(S,r), fort(C2), empty(S,C2), rebel(S,C1), withinRange(C1,C2). 

validOfficerMove(S,C1,C2) :- turn(S,o), empty(S,C2), officer(S,C1), withinRange(C1,C2).

validLeapMove(S,C1,C3) :- turn(S,o), empty(S,C3), officer(S,C1), rebel(S,C2), connected(C1,C2,C3).

connectedOfficer(S,C1,C2,C3) :- connected(C1,C2,C3), officer(S,C1).
officerExist(S,C1,C2,C3) :- connected(C1,C2,C3), officer(S,C1), empty(S,C3). 
rebelExist(S,C1,C2,C3) :- connected(C1,C2,C3), rebel(S,C2), empty(S,C3).
officerCanLeap(S,C1,C2,C3) :- connected(C1,C2,C3), officer(S,C1), rebel(S,C2), empty(S,C3).
forward(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X2<X1.
toFort(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X2<=X1.
rebelsWithinRange(S,C1,C2,C3) :- connected(C1,C2,C3), withinRange(C2,C4), rebel(S,C4).

toFort(cell(X1,Y1),cell(X2,Y2)) :- cell(X1,Y1), cell(X2,Y2), X2<=X1.


#modeh(rebelValidGoodMove(var(state),const(rank),var(cell),var(cell))).
#modeb(1,validRebelMove(var(state),var(cell),var(cell))).
#modeb(2,officerExist(var(state),_,var(cell),_)).
modeb(1,officerCanLeap(var(state),_,_,var(cell))).
#modeb(1,connectedOfficer(var(state),_,var(cell),var(cell))).
#modeb(1,officerExist(var(state),var(cell),var(cell),_)).
#modeb(1,forward(var(cell),var(cell))).
#modeb(1,fort(var(cell))).

#constant(rank,1).
#constant(rank,2).
#constant(rank,3).
#constant(rank,4).
#constant(rank,5).

#maxv(3).
#max_penalty(20).

%Learning Rank 1
#pos(p0, {
	rebelValidGoodMove(2,1,cell(4,3),cell(3,3)),
	rebelValidGoodMove(6,1,cell(2,1),cell(2,2))
}, {
	rebelValidGoodMove(6,1,cell(3,2),cell(2,2))
}, {
	turn(0,r).turn(1,o).turn(2,r).turn(3,o).turn(4,r).turn(5,o).turn(6,r).turn(7,o).
	officer(2,cell(1,3)).rebel(2,cell(2,3)).empty(2,cell(3,3)).rebel(2,cell(4,3)).
	officer(6,cell(2,4)).officer(6,cell(1,2)).rebel(6,cell(2,3)).empty(6,cell(2,2)).rebel(6,cell(3,2)).rebel(6,cell(2,1)).
}).

#pos(p1, {
	rebelValidGoodMove(2,1,cell(2,4),cell(2,3))
}, {
	rebelValidGoodMove(0,1,cell(3,4),cell(2,4)),
	rebelValidGoodMove(2,1,cell(2,4),cell(1,3)), rebelValidGoodMove(6,1,cell(1,3),cell(1,4)),
	rebelValidGoodMove(6,1,cell(1,3),cell(2,3))
}, {
	turn(0,r).turn(1,o).turn(2,r).turn(3,o).turn(4,r).turn(5,o).turn(6,r).turn(7,o).
	officer(2,cell(1,4)).officer(2,cell(0,2)).rebel(2,cell(2,4)).empty(2,cell(3,4)).empty(2,cell(2,3)).empty(2,cell(1,3)).
	officer(6,cell(1,2)).officer(6,cell(2,4)).rebel(6,cell(1,3)).empty(6,cell(1,4)).empty(6,cell(2,3)).empty(6,cell(2,2)).empty(6,cell(0,4)).
}).

%=============================

%Learning Rank 1,2, 3.1
#pos(p0, {
	rebelValidGoodMove(2,1,cell(4,3),cell(3,3)),
	rebelValidGoodMove(2,1,cell(3,2),cell(3,3)),
	rebelValidGoodMove(6,1,cell(2,1),cell(2,2))
}, {
	rebelValidGoodMove(6,1,cell(3,2),cell(2,2))
}, {
	turn(0,r).turn(1,o).turn(2,r).turn(3,o).turn(4,r).turn(5,o).turn(6,r).turn(7,o).
	officer(2,cell(1,3)).rebel(2,cell(2,3)).empty(2,cell(3,3)).rebel(2,cell(4,3)).rebel(2,cell(3,2)).
	officer(6,cell(2,4)).officer(6,cell(1,2)).rebel(6,cell(2,3)).empty(6,cell(2,2)).rebel(6,cell(3,2)).rebel(6,cell(2,1)).
}).

#pos(p1, {
	rebelValidGoodMove(2,1,cell(2,4),cell(2,3)),
	rebelValidGoodMove(6,1,cell(1,3),cell(0,4))
}, {
	rebelValidGoodMove(0,1,cell(3,4),cell(2,4)),
	rebelValidGoodMove(2,1,cell(2,4),cell(1,3)), rebelValidGoodMove(6,1,cell(1,3),cell(1,4)),
	rebelValidGoodMove(6,1,cell(1,3),cell(2,3))
}, {
	turn(0,r).turn(1,o).turn(2,r).turn(3,o).turn(4,r).turn(5,o).turn(6,r).turn(7,o).
	officer(2,cell(1,4)).officer(2,cell(0,2)).rebel(2,cell(2,4)).empty(2,cell(3,4)).empty(2,cell(2,3)).empty(2,cell(1,3)).
	officer(6,cell(1,2)).officer(6,cell(2,4)).rebel(6,cell(1,3)).empty(6,cell(1,4)).empty(6,cell(2,3)).empty(6,cell(2,2)).empty(6,cell(0,4)).
}).

#pos(p2, {
	rebelValidGoodMove(0,2,cell(3,3),cell(2,3)),
	rebelValidGoodMove(0,3,cell(2,1),cell(2,2)),
	rebelValidGoodMove(0,3,cell(3,3),cell(2,3))
}, {
	rebelValidGoodMove(0,2,cell(2,1),cell(2,2)),
	rebelValidGoodMove(2,2,cell(3,4),cell(2,4)),
	rebelValidGoodMove(2,2,cell(2,3),cell(1,3)),
	rebelValidGoodMove(2,3,cell(3,4),cell(2,4)),
	rebelValidGoodMove(2,3,cell(2,3),cell(1,3))
}, {
	turn(0,r).turn(1,o).turn(2,r).
	officer(2,cell(1,4)).empty(2,cell(2,4)).empty(2,cell(1,2)).empty(2,cell(1,3)).rebel(2,cell(2,3)).rebel(2,cell(3,4)).
}).

%=====Learning Rank 3.2,3.3, 4, 5====================
%move(0,cell(3,3),cell(2,3)).
%move(1,cell(0,4),cell(1,4)).
%move(2,cell(2,5),cell(2,4)).
%move(3,cell(1,4),cell(0,4)).
%move(4,cell(3,1),cell(2,2)).
%move(5,cell(0,2),cell(1,2)).
%move(6,cell(2,2),cell(1,3)).
%move(7,cell(1,2),cell(2,2)).
%move(8,cell(1,3),cell(0,2)).
%move(9,cell(2,2),cell(1,2)).
#pos(p3, {
	rebelValidGoodMove(4,3,cell(2,3),cell(1,3)),
	rebelValidGoodMove(4,3,cell(2,4),cell(1,4))
}, {
	rebelValidGoodMove(0,3,cell(3,3),cell(2,3)),
	rebelValidGoodMove(0,3,cell(3,2),cell(0,3)),
	rebelValidGoodMove(2,3,cell(2,3),cell(0,3)),
	rebelValidGoodMove(4,3,cell(3,2),cell(2,2)),
	rebelValidGoodMove(4,3,cell(2,3),cell(2,2)),
	rebelValidGoodMove(5,3,cell(2,3),cell(1,3)),
	rebelValidGoodMove(6,3,cell(2,3),cell(1,3)),
	rebelValidGoodMove(6,3,cell(3,2),cell(2,2)),
	rebelValidGoodMove(6,3,cell(2,4),cell(1,3)),
	rebelValidGoodMove(8,3,cell(1,3),cell(1,2)),
	rebelValidGoodMove(10,3,cell(0,2),cell(0,3))
}, {
	turn(0,r).turn(1,o).turn(2,r).turn(3,o).turn(4,r).turn(5,o).turn(6,r).turn(7,o).turn(8,r).turn(9,o).turn(10,r).
	rebel(2,cell(2,3)).empty(2,cell(0,3)).
	officer(4,cell(0,4)).empty(4,cell(1,3)).rebel(4,cell(2,3)).empty(4,cell(2,2)).rebel(4,cell(3,2)).rebel(4,cell(2,4)).empty(4,cell(1,4)).
	officer(5,cell(0,4)).empty(5,cell(1,3)).empty(5,cell(2,2)).rebel(5,cell(2,3)).
	officer(6,cell(0,4)).officer(6,cell(1,2)).empty(6,cell(1,3)).rebel(6,cell(2,2)).rebel(6,cell(2,3)).rebel(6,cell(3,2)).empty(6,cell(3,1)).rebel(6,cell(2,4)).
	officer(8,cell(2,2)).rebel(8,cell(1,3)).empty(8,cell(1,2)).empty(8,cell(0,2)).
	officer(10,cell(0,4)).empty(10,cell(0,3)).rebel(10,cell(0,2)).
}).

%move(0,cell(3,3),cell(2,3)).
%move(1,cell(0,4),cell(1,4)).
%move(2,cell(3,4),cell(3,3)).
%move(3,cell(1,4),cell(2,4)).
%move(4,cell(2,3),cell(1,3)).
%move(5,cell(2,4),cell(1,4)).
%move(6,cell(1,3),cell(0,4)).
%move(7,cell(1,4),cell(2,4)).
%move(8,cell(0,4),cell(1,3)).
%move(9,cell(0,2),cell(0,3)).

#pos(p4, {
	rebelValidGoodMove(2,3,cell(3,4),cell(2,4)),
	rebelValidGoodMove(4,3,cell(4,4),cell(3,4))
}, {
	rebelValidGoodMove(0,3,cell(3,2),cell(1,2)),
	rebelValidGoodMove(2,3,cell(2,4),cell(1,3)),
	rebelValidGoodMove(4,3,cell(3,3),cell(3,4)),
	rebelValidGoodMove(4,3,cell(2,2),cell(2,3)),
	rebelValidGoodMove(5,3,cell(4,4),cell(3,4)),
	rebelValidGoodMove(6,3,cell(1,3),cell(0,3)),
	rebelValidGoodMove(6,3,cell(0,4),cell(0,3)),
	rebelValidGoodMove(8,3,cell(0,4),cell(1,4)),
	rebelValidGoodMove(10,3,cell(1,3),cell(2,3))
}, {
	turn(0,r).turn(1,o).turn(2,r).turn(3,o).turn(4,r).turn(5,o).turn(6,r).turn(7,o).turn(8,r).turn(9,o).turn(10,r).
	officer(2,cell(1,4)).officer(2,cell(0,2)).empty(2,cell(1,3)).empty(2,cell(2,4)).rebel(2,cell(3,4)).rebel(2,cell(2,3)).validRebelMove(2,cell(2,3),cell(1,3)).empty(2,cell(1,2)).
	officer(4,cell(2,4)).rebel(4,cell(4,4)).rebel(4,cell(3,3)).empty(4,cell(3,4)).rebel(4,cell(2,3)).rebel(4,cell(2,2)).empty(4,cell(1,3)).
	officer(5,cell(2,4)).rebel(5,cell(4,4)).rebel(5,cell(3,3)).empty(5,cell(3,4)).
	officer(6,cell(0,2)).rebel(6,cell(1,3)).empty(6,cell(0,3)).empty(6,cell(0,4)).
	officer(8,cell(2,4)).empty(8,cell(1,4)).rebel(8,cell(0,4)).
	officer(10,cell(2,4)).empty(10,cell(2,3)).empty(10,cell(2,2)).rebel(10,cell(1,3)).
}).

%move(0,cell(3,3),cell(2,3))
%move(1,cell(0,4),cell(1,4))
#pos(p5, {
	rebelValidGoodMove(0,4,cell(3,3),cell(2,3)),
	rebelValidGoodMove(2,4,cell(2,3),cell(2,2)),
	rebelValidGoodMove(2,4,cell(2,3),cell(1,3))
}, {
	rebelValidGoodMove(0,4,cell(3,3),cell(0,3)),
	rebelValidGoodMove(2,4,cell(4,3),cell(3,3))
}, {
	turn(0,r).turn(1,o).turn(2,r).turn(3,o).turn(4,r).
	rebel(2,cell(2,3)).empty(2,cell(3,3)).empty(2,cell(2,2)).empty(2,cell(2,4)).empty(2,cell(1,3)).rebel(2,cell(4,3)).
}).

#pos(p6, {
	rebelValidGoodMove(0,5,cell(3,3),cell(2,3))
}, {
	rebelValidGoodMove(0,5,cell(4,3),cell(3,3))
}, {
	
}).

%move(0,cell(3,3),cell(2,3))
%move(1,cell(0,4),cell(1,4))
%move(2,cell(3,4),cell(3,3))
%move(3,cell(1,4),cell(2,4))
%move(4,cell(2,3),cell(1,3))
%move(5,cell(2,4),cell(3,4))
%move(6,cell(1,3),cell(1,4))
%move(7,cell(0,2),cell(0,3))
#pos(p7, {
	
}, {
	rebelValidGoodMove(8,3,cell(1,4),cell(2,4)),
	rebelValidGoodMove(8,3,cell(1,4),cell(0,4))
}, {
	turn(0,r).turn(1,o).turn(2,r).turn(3,o).turn(4,r).turn(5,o).turn(6,r).turn(7,o).turn(8,r).turn(9,o).turn(10,r).	
	officer(8,cell(3,4)).rebel(8,cell(1,4)).empty(8,cell(2,4)).empty(8,cell(0,4)).
}).

%=======================================

%====Officer Move Rank 1,2

#modeh(officerValidGoodMove(var(state),const(rank),var(cell),var(cell))).
#modeb(1,validLeapMove(var(state),var(cell),var(cell))).
#modeb(1,validOfficerMove(var(state),var(cell),var(cell))).
#modeb(1,forward(var(cell),var(cell)), (positive)).
#modeb(1,toFort(var(cell),var(cell)), (positive)).
#modeb(1,rebelExist(var(state),var(cell),_,var(cell))).
#modeb(1,fort(var(cell))).
#modeb(1,rebelsWithinRange(var(state),var(cell),_,_)).

#constant(rank,1).
#constant(rank,2).
#constant(rank,3).
#constant(rank,4).
#constant(rank,5).
#constant(rank,6).
#constant(rank,7).
#constant(rank,8).
#constant(rank,9).

#maxv(4).
#max_penalty(8).

%======Learning RANK 1,2,3==================
%move(0,cell(3,3),cell(2,3))
%move(1,cell(0,4),cell(1,4))
%move(2,cell(2,3),cell(1,3))
%move(3,cell(1,4),cell(2,4))
%move(4,cell(2,1),cell(2,2))
%move(5,cell(0,2),cell(0,3))
%move(6,cell(2,2),cell(1,2))
%move(7,cell(2,4),cell(1,4))
%move(8,cell(3,4),cell(2,4))
#pos(p8, {
	officerValidGoodMove(7,1,cell(2,4),cell(0,2)),
	officerValidGoodMove(7,2,cell(2,4),cell(0,2)),
	officerValidGoodMove(9,2,cell(1,4),cell(1,2)),
	officerValidGoodMove(7,3,cell(2,4),cell(0,2)),
	officerValidGoodMove(7,3,cell(2,4),cell(0,2)),
	officerValidGoodMove(7,3,cell(0,3),cell(2,3))
}, {
	officerValidGoodMove(7,1,cell(0,3),cell(2,3)),
	officerValidGoodMove(7,1,cell(2,4),cell(1,4)),
	officerValidGoodMove(7,3,cell(2,4),cell(1,4)),
	officerValidGoodMove(7,2,cell(0,3),cell(2,3)),
	officerValidGoodMove(9,1,cell(1,4),cell(1,2)),
	officerValidGoodMove(9,2,cell(1,4),cell(3,4))
}, {
	turn(0,r).turn(1,o).turn(2,r).turn(3,o).turn(4,r).turn(5,o).turn(6,r).turn(7,o).turn(8,r).turn(9,o).turn(10,r).	
	officer(7,cell(0,3)).officer(7,cell(2,4)).rebel(7,cell(1,3)).empty(7,cell(0,2)).empty(7,cell(2,3)).
	officer(9,cell(1,4)).rebel(9,cell(1,3)).empty(9,cell(1,2)).rebel(9,cell(2,4)).empty(9,cell(3,4)).
}).

%======Learning Rank 7,8,9===================
%move(0,cell(3,3),cell(2,3))
%move(1,cell(0,4),cell(1,4))
%move(2,cell(3,4),cell(3,3))
%move(3,cell(1,4),cell(2,4))
%move(4,cell(2,3),cell(2,2))
%move(5,cell(2,4),cell(3,4))
%move(6,cell(3,3),cell(2,3))
#pos(p9, {
	officerValidGoodMove(5,7,cell(2,4),cell(2,3)),
	officerValidGoodMove(5,7,cell(2,4),cell(1,4)),
	officerValidGoodMove(5,8,cell(2,4),cell(1,4)),
	officerValidGoodMove(5,9,cell(2,4),cell(2,3))
}, {
	officerValidGoodMove(5,7,cell(2,4),cell(3,4)),
	officerValidGoodMove(7,7,cell(3,4),cell(3,3)),
	officerValidGoodMove(5,8,cell(2,4),cell(2,3))
}, {
	turn(0,r).turn(1,o).turn(2,r).turn(3,o).turn(4,r).turn(5,o).turn(6,r).turn(7,o).turn(8,r).turn(9,o).turn(10,r).	
	officer(5,cell(2,4)).empty(5,cell(3,4)).empty(5,cell(2,3)).empty(5,cell(1,4)).
	officer(7,cell(3,4)).empty(7,cell(3,3)).
}).

%Learning Rank 4,6=======================
%move(0,cell(3,1),cell(2,2))
%move(1,cell(0,4),cell(1,4))
%move(2,cell(2,2),cell(1,3))
%move(3,cell(1,4),cell(2,4))
%move(4,cell(2,1),cell(2,2))
#pos(p10, {
	officerValidGoodMove(5,4,cell(2,4),cell(2,3)),
	officerValidGoodMove(5,4,cell(0,2),cell(1,2)),
	officerValidGoodMove(5,4,cell(2,4),cell(1,4)),
	officerValidGoodMove(5,6,cell(2,4),cell(2,3)),
	officerValidGoodMove(5,6,cell(0,2),cell(1,2)),
	officerValidGoodMove(5,6,cell(2,4),cell(1,4)),
	officerValidGoodMove(5,6,cell(0,2),cell(0,3))
}, {
	officerValidGoodMove(3,4,cell(0,2),cell(1,2)),
	officerValidGoodMove(5,4,cell(0,2),cell(0,3)),
	officerValidGoodMove(3,6,cell(0,2),cell(1,2))
}, {
	turn(0,r).turn(1,o).turn(2,r).turn(3,o).turn(4,r).turn(5,o).turn(6,r).turn(7,o).turn(8,r).turn(9,o).turn(10,r).	
	officer(3,cell(0,2)).officer(3,cell(1,4)).rebel(3,cell(1,3)).empty(3,cell(1,2)).
	officer(5,cell(2,4)).officer(5,cell(0,2)).rebel(5,cell(1,3)).empty(5,cell(0,3)).empty(5,cell(2,3)).empty(5,cell(1,4)).empty(5,cell(1,2)).
}).


%============================================
%====LEARNING RANK 5 SPECIAL ======================
#pos(p11, {
	officerValidGoodMove(1,5,cell(0,4),cell(1,3))
}, {
	officerValidGoodMove(1,5,cell(0,4),cell(0,3))
}, {
	turn(0,r).turn(1,o).turn(2,r).turn(3,o).turn(4,r).turn(5,o).turn(6,r).turn(7,o).turn(8,r).turn(9,o).turn(10,r).
	officer(1,cell(0,4)).empty(1,cell(0,3)).empty(1,cell(1,3)).empty(1,cell(1,4)).empty(1,cell(2,4)).empty(1,cell(2,3)).empty(1,cell(2,2)).rebel(1,cell(3,3)).
	officer(3,cell(3,3)).empty(3,cell(2,3)).empty(3,cell(1,3)).empty(3,cell(0,3)).empty(3,cell(2,2)).empty(3,cell(2,4)).empty(3,cell(2,1)).empty(3,cell(2,5)).
}).
%==============================================