cell(1..9).
empty(0,cell(1..9)).
center(cell(5)).
corners(cell(2)).
corners(cell(4)).
corners(cell(6)).
corners(cell(8)).

%#const max=10. 

turn(0,c).
turn(S,c) :- turn(S-1,n), S<10.
turn(S,n) :- turn(S-1,c), S<10.

validMove(S, cell(X),P) :- turn(S,P), empty(S,cell(X)).

0 {move(S,cell(X),P)} 1 :- validMove(S,cell(X),P).

%changed(S,cell(X)) :- move(S,cell(X),P).

noughts(S+1, cell(X)) :- move(S,cell(X),n), S<10, not finished(S).
noughts(S+1, cell(X)) :- noughts(S,cell(X)), S<10, not finished(S).
crosses(S+1, cell(X)) :- move(S,cell(X),c), S<10, not finished(S).
crosses(S+1, cell(X)) :- crosses(S,cell(X)), S<10, not finished(S).
empty(S+1, cell(X)) :- empty(S,cell(X)), not move(S,cell(X),_), S<10, not finished(S).

:- move(S, cell(X1),_), move(S,cell(X2),_), X1 != X2.
:- empty(S,_), not move(S,_,_).

wins(S,n) :- noughts(S,cell(X)), noughts(S,cell(Y)), noughts(S,cell(Z)), X+Y+Z=15, X!=Y, Y!=Z, X!=Z.
wins(S,c) :- crosses(S,cell(X)), crosses(S,cell(Y)), crosses(S,cell(Z)), X+Y+Z=15, X!=Y, Y!=Z, X!=Z.

draw(S) :- not wins(S,_), not empty(S,_), crosses(S,_).

finished(S) :- draw(S).
finished(S) :- wins(S,P).

%:- wins(S,n).
%:- not finished(_).

threePlus(A+B+C) :- cell(A), cell(B), cell(C).
lessThan(A,B) :- cell(A), cell(B), A<B.
equal(A,B) :- cell(A), cell(B), A=B.

%Original check()
%canWin(S,cell(X),n) :- empty(S,cell(X)), noughts(S,cell(Y)), noughts(S,cell(Z)), X+Y+Z=15, Y!=Z.
%canWin(S,cell(X),c) :- empty(S,cell(X)), crosses(S,cell(Y)), crosses(S,cell(Z)), X+Y+Z=15, Y!=Z.

%canCheck(S,cell(X),cell(Y),n) :- empty(S,cell(X)), empty(S,cell(Y)), noughts(S,cell(Z)), X+Y+Z=15, X!=Y. 
%canCheck(S,cell(X),cell(Y),c) :- empty(S,cell(X)), empty(S,cell(Y)), crosses(S,cell(Z)), X+Y+Z=15, X!=Y. 

%canFork(S,C,P) :- canCheck(S,C,C2,P), canCheck(S,C,C3,P), C2!=C3.

%blockWin(S,C) :- canWin(S,C,n), validMove(S,C,c), turn(S,c).
%blockWin(S,C) :- canWin(S,C,c), validMove(S,C,n), turn(S,n).


#modeh(a(var(state),cell(var(c)),const(p))).


%#modeb(1,center(cell(var(c)))).
%#modeb(1,corner(cell(var(c)))).
%#modeb(1,turn(var(state),cell(var(c)))).
%#modeb(1,validMove(var(state),cell(var(c)),var(player))).
%#modeb(1,move(var(state),cell(var(c)),var(player))).
%#modeb(2,noughts(var(state),cell(var(c)))).
#modeb(2,crosses(var(state),cell(var(c))), (positive)).
#modeb(1,empty(var(state),cell(var(c))), (positive)).
%#modeb(1,wins(var(state),const(p))).
%#modeb(1,draw(var(state),const(p))).
#modeb(1,threePlus(const(sum)), (positive)).
#modeb(1,lessThan(var(c),var(c)), (anti_reflexive, symmetric, positive)).
%#modeb(1,equal(var(c),var(c))).

%#constant(p,n).
#constant(p,c).
#constant(sum,15).

#maxv(4).
#max_penalty(8).

%#pos(p0, {a(4,cell(8),c),a(3,cell(8),c)}, {a(4,cell(1),c),a(2,cell(8),c),a(4,cell(9),c),a(0,cell(8),c),a()}, {empty(0,cell(4)).empty(0,cell(3)).empty(0,cell(8)).empty(0,cell(9)).empty(0,cell(5)).empty(0,cell(1)).empty(0,cell(2)).empty(0,cell(7)).empty(0,cell(6)).crosses(1,cell(4)).empty(1,cell(3)).empty(1,cell(8)).empty(1,cell(9)).empty(1,cell(5)).empty(1,cell(1)).empty(1,cell(2)).empty(1,cell(7)).empty(1,cell(6)).crosses(2,cell(4)).empty(2,cell(3)).empty(2,cell(8)).noughts(2,cell(9)).empty(2,cell(5)).empty(2,cell(1)).empty(2,cell(2)).empty(2,cell(7)).empty(2,cell(6)).crosses(3,cell(4)).crosses(3,cell(3)).empty(3,cell(8)).noughts(3,cell(9)).empty(3,cell(5)).empty(3,cell(1)).empty(3,cell(2)).empty(3,cell(7)).empty(3,cell(6)).crosses(4,cell(4)).crosses(4,cell(3)).empty(4,cell(8)). noughts(4,cell(9)).noughts(4,cell(5)). empty(4,cell(1)).empty(4,cell(2)).empty(4,cell(7)). empty(4,cell(6)).}).


#pos(p4, {a(4,cell(6),c)}, {a(4,cell(1),c),a(4,cell(9),c),a(4,cell(2),c),a(4,cell(7),c)}, {empty(0,cell(4)).empty(0,cell(3)).empty(0,cell(8)).empty(0,cell(9)).empty(0,cell(5)).empty(0,cell(1)).empty(0,cell(2)).empty(0,cell(7)).empty(0,cell(6)).crosses(1,cell(4)).empty(1,cell(3)).empty(1,cell(8)).empty(1,cell(9)).empty(1,cell(5)).empty(1,cell(1)).empty(1,cell(2)).empty(1,cell(7)).empty(1,cell(6)).crosses(2,cell(4)).noughts(2,cell(3)).empty(2,cell(8)).empty(2,cell(9)).empty(2,cell(5)).empty(2,cell(1)).empty(2,cell(2)).empty(2,cell(7)).empty(2,cell(6)).crosses(3,cell(4)).noughts(3,cell(3)).empty(3,cell(8)).empty(3,cell(9)).crosses(3,cell(5)).empty(3,cell(1)).empty(3,cell(2)).empty(3,cell(7)).empty(3,cell(6)).crosses(4,cell(4)).noughts(4,cell(3)).noughts(4,cell(8)).empty(4,cell(9)).crosses(4,cell(5)).empty(4,cell(1)).empty(4,cell(2)).empty(4,cell(7)).empty(4,cell(6)).}).
