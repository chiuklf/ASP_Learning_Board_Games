cell(1..9).
empty(0,cell(1..9)).
center(cell(5)).
corners(cell(2)).
corners(cell(4)).
corners(cell(6)).
corners(cell(8)).

player(n).
player(c). 

turn(0,c).
turn(S,c) :- turn(S-1,n), S<10.
turn(S,n) :- turn(S-1,c), S<10.

validMove(S, cell(X),P) :- turn(S,P), empty(S,cell(X)).

0 {move(S,cell(X),P)} 1 :- validMove(S,cell(X),P).

changed(S,cell(X)) :- move(S,cell(X),P).

noughts(S+1, cell(X)) :- move(S,cell(X),n), S<10.
noughts(S+1, cell(X)) :- noughts(S,cell(X)), S<10.
crosses(S+1, cell(X)) :- move(S,cell(X),c), S<10.
crosses(S+1, cell(X)) :- crosses(S,cell(X)), S<10.
empty(S+1, cell(X)) :- empty(S,cell(X)), not changed(S,cell(X)), S<10.

:- move(S, cell(X1),_), move(S,cell(X2),_), X1 != X2.
%:- empty(S,_), not move(S,_,_).

notEqualPlayer(A,B) :- player(A), player(B), A!=B.

opposite(cell(X),cell(Y)) :- cell(X), cell(Y), X+Y=10, X!=Y.

%Original check()
canWin(S,cell(X),n) :- empty(S,cell(X)), noughts(S,cell(Y)), noughts(S,cell(Z)), X+Y+Z=15, Y!=Z.
canWin(S,cell(X),c) :- empty(S,cell(X)), crosses(S,cell(Y)), crosses(S,cell(Z)), X+Y+Z=15, Y!=Z.


canCheck(S,cell(X),cell(Y),n) :- empty(S,cell(X)), empty(S,cell(Y)), noughts(S,cell(Z)), X+Y+Z=15, X!=Y. 
canCheck(S,cell(X),cell(Y),c) :- empty(S,cell(X)), empty(S,cell(Y)), crosses(S,cell(Z)), X+Y+Z=15, X!=Y. 

canFork(S,C,P) :- canCheck(S,C,C2,P), canCheck(S,C,C3,P), C2!=C3.

blockWin(S,C,c) :- canWin(S,C,n).
blockWin(S,C,n) :- canWin(S,C,c).

#modeh(validgoodMove(var(state), const(rank), var(cell), var(player))).
#modeh(validgoodMove(const(s), const(rank), var(cell), var(player))).
#modeh(validgoodMove(var(state), const(rank), var(cell), const(p))).

#modeb(1,canWin(var(state),var(cell),var(player))).
#modeb(1,validMove(var(state),var(cell),var(player)), (positive)).
#modeb(1,blockWin(var(state),var(cell),var(player))).
#modeb(1,canFork(var(state),var(cell),var(player))).
#modeb(1,canCheck(var(state),var(cell),var(cell),var(player)), (positive)).
#modeb(1,notEqualPlayer(var(player),var(player)),(positive)).
#modeb(1,corners(var(cell))).
#modeb(1,center(var(cell))).
#modeb(1,validMove(var(state),var(cell),const(p)), (positive)).
#modeb(1,validMove(const(s),var(cell),var(player)), (positive)).
#modeb(1,crosses(var(state),var(cell))).
#modeb(1,noughts(var(state),var(cell))).
#modeb(1,opposite(var(cell),var(cell)), (positive)).

#constant(rank,1).
#constant(rank,2).
#constant(rank,3).
#constant(rank,4).
#constant(rank,5).
#constant(rank,6).
#constant(rank,7).
#constant(rank,8).
#constant(rank,9).
#constant(s,0).
#constant(p,c).
#constant(p,n).


#maxv(4).
#max_penalty(25).

%Examples for Rule 1,2 and 3
#pos(p0, {
	validgoodMove(3,2,cell(5),n), validgoodMove(4,1,cell(5),c), validgoodMove(4,3,cell(8),c), validgoodMove(5,2,cell(8),n)
}, {
	validgoodMove(3,1,cell(8),n), validgoodMove(3,1,cell(5),n), 
	validgoodMove(3,1,cell(5),c), validgoodMove(3,3,cell(8),c), validgoodMove(3,3,cell(5),n), validgoodMove(4,2,cell(8),c), validgoodMove(4,3,cell(8),n), validgoodMove(4,2,cell(5),n), validgoodMove(4,1,cell(8),c), validgoodMove(3,2,cell(7),n), validgoodMove(4,2,cell(1),n)
}, {

	move(0,cell(4),c).move(1,cell(9),n).move(2,cell(6),c).move(3,cell(2),n).move(4,cell(3),c).
}).

#pos(p8, {
	validgoodMove(4,3,cell(5),c)
}, {

}, {
	turn(0,c).turn(1,n).turn(2,c).turn(3,n).turn(4,c).
	move(0,cell(4),c).move(1,cell(9),n).move(2,cell(8),c).move(3,cell(1),n).
}).

%===============================

%Examples for Rule 4.1
#pos(p1, {
	validgoodMove(5,4,cell(1),n), validgoodMove(5,4,cell(9),n) 
}, {
	validgoodMove(5,4,cell(7),n), validgoodMove(5,4,cell(2),n), validgoodMove(5,4,cell(9),c), validgoodMove(5,4,cell(1),c)
}, {
	move(0,cell(4),c).move(1,cell(3),n).move(2,cell(8),c).move(3,cell(5),n).move(4,cell(6),c).
}).

#pos(p9, {
	validgoodMove(3,4,cell(6),n), validgoodMove(3,4,cell(9),n), validgoodMove(3,4,cell(4),n)
}, {
	validgoodMove(3,4,cell(5),n), validgoodMove(3,4,cell(8),n)
}, {
	move(0,cell(3),c).move(1,cell(2),n).move(2,cell(1),c).
}).

#pos(p2, {
	validgoodMove(3,4,cell(8),n)
}, {
	validgoodMove(3,4,cell(5),n), validgoodMove(3,4,cell(2),n)
}, {
	move(0,cell(4),c).move(1,cell(2),n).move(2,cell(6),c).
}).

%==================================

%Examples for Rule 4.2
#pos(p3, {
	validgoodMove(3,4,cell(8),n), validgoodMove(4,4,cell(5),c)
}, {
	validgoodMove(3,4,cell(5),n), validgoodMove(3,4,cell(2),n), validgoodMove(4,4,cell(8),c), validgoodMove(4,4,cell(3),c)
}, {
	move(0,cell(4),c).move(1,cell(7),n).move(2,cell(6),c).move(3,cell(9),n).
}).

#pos(p4, {
	validgoodMove(4,4,cell(5),c), validgoodMove(4,4,cell(5),c)
}, {
	validgoodMove(4,4,cell(9),c), validgoodMove(4,4,cell(5),n)
}, {
	move(0,cell(3),c).move(1,cell(2),n).move(2,cell(1),c).move(3,cell(6),n).	
}).

%===============================

%Example for Rule 5
#pos(p5, {
	validgoodMove(0,5,cell(2),c), validgoodMove(0,5,cell(4),c), validgoodMove(0,5,cell(6),c), validgoodMove(0,5,cell(8),c)
}, {
	validgoodMove(0,5,cell(1),c), validgoodMove(0,5,cell(3),c), validgoodMove(0,5,cell(5),c), validgoodMove(0,5,cell(7),c),
	validgoodMove(0,5,cell(9),c), validgoodMove(1,5,cell(1),n),
	validgoodMove(1,5,cell(2),n), validgoodMove(1,5,cell(3),n), validgoodMove(1,5,cell(4),n), validgoodMove(1,5,cell(5),n), validgoodMove(1,5,cell(6),n), validgoodMove(1,5,cell(7),n), validgoodMove(1,5,cell(8),n), validgoodMove(1,5,cell(9),n), validgoodMove(2,5,cell(1),c), validgoodMove(2,5,cell(2),c), validgoodMove(2,5,cell(3),c), validgoodMove(2,5,cell(4),c), validgoodMove(2,5,cell(5),c), validgoodMove(2,5,cell(6),c), validgoodMove(2,5,cell(7),c), validgoodMove(2,5,cell(8),c), validgoodMove(2,5,cell(9),c)  
}, {
	move(0,cell(1),c).move(1,cell(2),n).move(2,cell(6),c).
}).

%Example for Rule 6,8,9
#pos(p6, {
	validgoodMove(0,6,cell(5),c), validgoodMove(1,6,cell(5),n),
	validgoodMove(0,8,cell(2),c), validgoodMove(1,8,cell(2),n),
	validgoodMove(0,9,cell(1),c), validgoodMove(1,9,cell(3),n)
}, {
	validgoodMove(0,6,cell(1),c), validgoodMove(0,6,cell(2),c), validgoodMove(0,6,cell(3),c), validgoodMove(0,6,cell(4),c), validgoodMove(0,6,cell(6),c), validgoodMove(0,6,cell(7),c), validgoodMove(0,6,cell(8),c), validgoodMove(0,6,cell(9),c), 	validgoodMove(1,6,cell(2),n), validgoodMove(0,8,cell(5),c), 	validgoodMove(0,8,cell(1),c), validgoodMove(1,8,cell(5),n), 	validgoodMove(0,9,cell(5),c), validgoodMove(0,9,cell(2),c), 	validgoodMove(1,9,cell(5),n), validgoodMove(1,9,cell(1),n)
}, {
	move(0,cell(1),c).
}).

%===============================

%Example for Rule 7
#pos(p7, {
	validgoodMove(1,7,cell(6),n), validgoodMove(2,7,cell(2),c)
}, {
	validgoodMove(0,7,cell(1),c), validgoodMove(0,7,cell(2),c), validgoodMove(0,7,cell(3),c), validgoodMove(0,7,cell(4),c), validgoodMove(0,7,cell(5),c), validgoodMove(0,7,cell(6),c), validgoodMove(0,7,cell(7),c), validgoodMove(0,7,cell(8),c), validgoodMove(0,7,cell(9),c), validgoodMove(1,7,cell(1),n), validgoodMove(1,7,cell(2),n), validgoodMove(1,7,cell(3),n), validgoodMove(1,7,cell(4),n), validgoodMove(1,7,cell(5),n), validgoodMove(1,7,cell(7),n), validgoodMove(1,7,cell(8),n), validgoodMove(1,7,cell(9),n), validgoodMove(1,7,cell(6),c), validgoodMove(2,7,cell(6),c), validgoodMove(3,7,cell(1),n), validgoodMove(3,7,cell(2),n), validgoodMove(3,7,cell(5),n), validgoodMove(3,7,cell(2),c), validgoodMove(4,7,cell(3),c), validgoodMove(4,7,cell(5),c), validgoodMove(4,7,cell(6),n)
}, {
	move(0,cell(4),c).move(1,cell(8),n).move(2,cell(9),c).move(3,cell(7),n).move(4,cell(2),c).
}).
