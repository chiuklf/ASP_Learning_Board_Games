import subprocess

finish = False
turn = 0
player = "c"

with open("newNandC.lp", "a") as myfile:
	while(not finish):
		num = input("Enter your move: ")


		if(turn%2==0):
			player = "c"
		else:
			player = "n"

		myfile.write("move("+str(turn)+",cell("+str(num)+"),"+player+").\n")
		myfile.flush()
		turn += 1
		if (turn == 9): 
			finish = True

		output = subprocess.Popen(["clingo", "--quiet=1", "newNandC.lp"], 
                          stdout=subprocess.PIPE).communicate()[0]

		stri = "Answer"
		index = output.find(stri)
		startIndex = output.find("\n",index)+1
		endIndex = output.find("\n",startIndex+1)
		model = output[startIndex:endIndex]

		moveIndex = model.find("move("+str(turn))
		moveEndIndex = model.find(" ", moveIndex+1)

		move = model[moveIndex:moveEndIndex]

		print move 
		myfile.write(move+".\n")
		myfile.flush()
		turn += 1

		if (turn == 9): 
			finish = True

myfile.close()