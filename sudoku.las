
cell(1..9).
e(1..2).
f(3..4).
e(5..6).
f(7..8).

test(C1,C2) :- e(C1), f(C2).

help(C1,C2) :- cell(C1), cell(C2), C1<C2.
threePlus(A+B+C) :- cell(A), cell(B), cell(C).

%a(C1,C2) :- test(C1,C2), C1<C2.

#modeh(a(var(cell),var(cell))).
#modeb(1,test(var(cell),var(cell))).
#modeb(1,help(var(cell),var(cell))).
%#modeb(1,var(cell) < var(cell)).


#maxv(10).
#max_penalty(10).


#pos(p0, {a(1,3),a(2,3)}, {a(5,3),a(3,2),a(3,7)}).
#neg(n0, {a(3,2)},{}).


%test(C1,C2,C3) :- e(C1), f(C2), f(C3).

%#modeh(a(cell(var(cell)))).

%#modeb(1,test(cell(var(cell)),cell(var(cell)),cell(var(cell)))).
%#modeb(1,var(cell)<var(cell)).


%#maxv(4).
%#max_penalty(10).
%#pos(p0, {a(cell(8))}, {a(cell(1))}, {e(cell(1)).e(cell(2)).e(cell(3)).e(cell(4)).f(cell(5)).f(cell(6)).f(cell(7)).f(cell(8)).f(cell(9)).}).


3 ~ a(C1,C2) :- test(C1,C2), 1<2.