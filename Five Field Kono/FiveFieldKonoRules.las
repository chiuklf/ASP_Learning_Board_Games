
diagonal(cell(R1,C1),cell(R2,C2)) :- board(_,cell(R1,C1),_), board(_,cell(R2,C2),_), R2=R1+1, C2=C1+1.
diagonal(cell(R1,C1),cell(R2,C2)) :- board(_,cell(R1,C1),_), board(_,cell(R2,C2),_), R2=R1+1, C2=C1-1.
diagonal(cell(R1,C1),cell(R2,C2)) :- board(_,cell(R1,C1),_), board(_,cell(R2,C2),_), R2=R1-1, C2=C1+1.
diagonal(cell(R1,C1),cell(R2,C2)) :- board(_,cell(R1,C1),_), board(_,cell(R2,C2),_), R2=R1-1, C2=C1-1.


%validMove(S,cell(R1,C1),cell(R2,C2),P) :- turn(S,P), diagonal(cell(R1,C1),cell(R2,C2)), board(S,cell(R1,C1),P), board(S,cell(R2,C2),e).


#modeh(validMove(var(state), var(cell), var(cell), var(players))).

#modeb(1,turn(var(state), var(players))).
#modeb(1,diagonal(var(cell), var(cell))).
#modeb(1,board(var(state), var(cell), var(players))).
#modeb(1,board(var(state), var(cell), const(piece))).

#constant(piece, e).

#maxv(4).
#max_penalty(6).

#pos(p0, {
	validMove(0,cell(0,0),cell(1,1),r),validMove(0,cell(0,1),cell(1,2),r),validMove(0,cell(0,2),cell(1,1),r),validMove(0,cell(0,2),cell(1,3),r),validMove(0,cell(0,3),cell(1,2),r),validMove(0,cell(0,4),cell(1,3),r),validMove(0,cell(1,0),cell(2,1),r),validMove(0,cell(1,4),cell(2,3),r),validMove(1,cell(3,4),cell(2,3),b),validMove(1,cell(4,0),cell(3,1),b),validMove(1,cell(4,1),cell(3,2),b),validMove(1,cell(4,2),cell(3,1),b),validMove(1,cell(4,2),cell(3,3),b),validMove(1,cell(4,3),cell(3,2),b),validMove(1,cell(4,4),cell(3,3),b)
},{
	validMove(0,cell(1,1),cell(2,2),r),validMove(1,cell(3,0),cell(2,1),b),validMove(0,cell(3,0),cell(2,1),b),validMove(0,cell(0,0),cell(2,2),r)
},{
	board(0,cell(0,0),r).board(0,cell(0,1),r).board(0,cell(0,2),r).board(0,cell(0,3),r).board(0,cell(0,4),r).board(0,cell(1,0),r).board(0,cell(1,1),e).board(0,cell(1,2),e).board(0,cell(1,3),e).board(0,cell(1,4),r).board(0,cell(2,0),e).board(0,cell(2,1),e).board(0,cell(2,2),e).board(0,cell(2,3),e).board(0,cell(2,4),e).board(0,cell(3,0),b).board(0,cell(3,1),e).board(0,cell(3,2),e).board(0,cell(3,3),e).board(0,cell(3,4),b).board(0,cell(4,0),b).board(0,cell(4,1),b).board(0,cell(4,2),b).board(0,cell(4,3),b).board(0,cell(4,4),b).board(1,cell(0,0),r).board(1,cell(0,1),r).board(1,cell(0,2),r).board(1,cell(0,3),r).board(1,cell(0,4),r).board(1,cell(1,0),e).board(1,cell(1,1),e).board(1,cell(1,2),e).board(1,cell(1,3),e).board(1,cell(1,4),r).board(1,cell(2,0),e).board(1,cell(2,1),r).board(1,cell(2,2),e).board(1,cell(2,3),e).board(1,cell(2,4),e).board(1,cell(3,0),b).board(1,cell(3,1),e).board(1,cell(3,2),e).board(1,cell(3,3),e).board(1,cell(3,4),b).board(1,cell(4,0),b).board(1,cell(4,1),b).board(1,cell(4,2),b).board(1,cell(4,3),b).board(1,cell(4,4),b).turn(1,b).turn(0,r).
}).

%#pos(p0, {validMove(0,cell(0,0),cell(1,1),r),validMove(0,cell(0,1),cell(1,2),r),validMove(0,cell(0,2),cell(1,1),r),validMove(0,cell(0,2),cell(1,3),r),validMove(0,cell(0,3),cell(1,2),r),validMove(0,cell(0,4),cell(1,3),r),validMove(0,cell(1,0),cell(2,1),r),validMove(0,cell(1,4),cell(2,3),r),validMove(1,cell(3,4),cell(2,3),b),validMove(1,cell(4,0),cell(3,1),b),validMove(1,cell(4,1),cell(3,2),b),validMove(1,cell(4,2),cell(3,1),b),validMove(1,cell(4,2),cell(3,3),b),validMove(1,cell(4,3),cell(3,2),b),validMove(1,cell(4,4),cell(3,3),b)},{validMove(0,cell(4,2),cell(3,3),b),validMove(0,cell(0,1),cell(1,0),r),validMove(0,cell(1,1),cell(2,2),r)},{board(0,cell(0,0),r).board(0,cell(0,1),r).board(0,cell(0,2),r).board(0,cell(0,3),r).board(0,cell(0,4),r).board(0,cell(1,0),r).board(0,cell(1,1),e).board(0,cell(1,2),e).board(0,cell(1,3),e).board(0,cell(1,4),r).board(0,cell(2,0),e).board(0,cell(2,1),e).board(0,cell(2,2),e).board(0,cell(2,3),e).board(0,cell(2,4),e).board(0,cell(3,0),b).board(0,cell(3,1),e).board(0,cell(3,2),e).board(0,cell(3,3),e).board(0,cell(3,4),b).board(0,cell(4,0),b).board(0,cell(4,1),b).board(0,cell(4,2),b).board(0,cell(4,3),b).board(0,cell(4,4),b).board(1,cell(0,0),r).board(1,cell(0,1),r).board(1,cell(0,2),r).board(1,cell(0,3),r).board(1,cell(0,4),r).board(1,cell(1,0),e).board(1,cell(1,1),e).board(1,cell(1,2),e).board(1,cell(1,3),e).board(1,cell(1,4),r).board(1,cell(2,0),e).board(1,cell(2,1),r).board(1,cell(2,2),e).board(1,cell(2,3),e).board(1,cell(2,4),e).board(1,cell(3,0),b).board(1,cell(3,1),e).board(1,cell(3,2),e).board(1,cell(3,3),e).board(1,cell(3,4),b).board(1,cell(4,0),b).board(1,cell(4,1),b).board(1,cell(4,2),b).board(1,cell(4,3),b).board(1,cell(4,4),b).turn(1,b).turn(0,r).}).
