cell(cell(0..4,0..4)).

board(0,C,r) :- redStart(C).
board(0,C,b) :- blueStart(C).
board(0,C,e) :- cell(C), not redStart(C), not blueStart(C).

redStart(cell(0,0..4)).
redStart(cell(1,0)).
redStart(cell(1,4)).
blueStart(cell(3,0)).
blueStart(cell(3,4)).
blueStart(cell(4,0..4)).

turn(0,r).

% Two players
player(r).
player(b).

turn(S,r) :- turn(S-1,b), board(S,_,_), not finished(S).
turn(S,b) :- turn(S-1,r), board(S,_,_), not finished(S).

%withinRange(R2, C2) :- board(S,R2,C2,P), R2>=0, R2<=4, C2>=0, C2<=4.

% Valid Move
% validMove(state, row1, col1, row2, col2, player)
validMove(S,cell(R1,C1),cell(R2,C2),P) :- turn(S,P), diagonal(cell(R1,C1),cell(R2,C2)), board(S,cell(R1,C1),P), board(S,cell(R2,C2),e).


0 {move(S,cell(R1,C1),cell(R2,C2),P)} 1 :- validMove(S,cell(R1,C1),cell(R2,C2),P).

diagonal(cell(R1,C1),cell(R2,C2)) :- board(_,cell(R1,C1),_), board(_,cell(R2,C2),_), R2=R1+1, C2=C1+1.
diagonal(cell(R1,C1),cell(R2,C2)) :- board(_,cell(R1,C1),_), board(_,cell(R2,C2),_), R2=R1+1, C2=C1-1.
diagonal(cell(R1,C1),cell(R2,C2)) :- board(_,cell(R1,C1),_), board(_,cell(R2,C2),_), R2=R1-1, C2=C1+1.
diagonal(cell(R1,C1),cell(R2,C2)) :- board(_,cell(R1,C1),_), board(_,cell(R2,C2),_), R2=R1-1, C2=C1-1.

change(S,C1) :- move(S,C1,_,P).
change(S,C2) :- move(S,_,C2,P).

board(S+1,C1,e) :- move(S,C1,_,P), S<80.
board(S+1,C2,P) :- move(S,_,C2,P), S<80.
board(S+1,C2,P) :- board(S,C2,P), not change(S,C2), S<80.

wins(S,b) :- board(S,cell(0,0),b),
	board(S,cell(0,1),b),
	board(S,cell(0,2),b),
	board(S,cell(0,3),b),
	board(S,cell(0,4),b),
	board(S,cell(1,0),b),
	board(S,cell(1,4),b).

wins(S,r) :- board(S,cell(3,0),r),
	board(S,cell(3,4),r),
	board(S,cell(4,0),r),
	board(S,cell(4,1),r),
	board(S,cell(4,2),r),
	board(S,cell(4,3),r),
	board(S,cell(4,4),r).

finished(S) :- wins(S,_).

lessThanCell(cell(X1,Y1),cell(X2,Y2)) :- cell(cell(X1,Y1)), cell(cell(X2,Y2)), X1<X2.
greaterThanCell(cell(X1,Y1),cell(X2,Y2)) :- cell(cell(X1,Y1)), cell(cell(X2,Y2)), X1>X2.
notEqualPlayer(P1,P2) :- player(P1), player(P2), P1!=P2.
forward(r,C1,C2) :- lessThanCell(C1,C2).
forward(b,C1,C2) :- greaterThanCell(C1,C2).

canWin(C2,r) :- blueStart(C2). 
canWin(C2,b) :- redStart(C2).

%canBlock(S,C1,C2,P) :- validMove(S,C1,C2,P), diagonal(C2,C3), board(S,C3,P2), P!=P2, player(P2).
%blocking(S,C1,P) :- board(S,C1,P), board(S,C2,P2), diagonal(C1,C2), P!=P2, player(P), player(P2).
canBlock(S,C2,P) :- diagonal(C2,C3), board(S,C3,P2), P!=P2, player(P), player(P2).

#modeh(validgoodMove(var(state),const(rank),var(cell),var(cell),var(player))).
#modeb(1,canWin(var(cell),var(player))).
#modeb(1,canBlock(var(state),var(cell),var(player))).
#modeb(1,forward(var(player),var(cell),var(cell)), (positive)).
#modeb(1,validMove(var(state),var(cell),var(cell),var(player))).


#constant(rank,1).
#constant(rank,2).
%#constant(rank,3).
#constant(rank,4).
#constant(rank,5).
#constant(rank,6).

#maxv(4).
#max_penalty(16).

#pos(p0, {
	validgoodMove(6,1,cell(3,2),cell(4,3),r), validgoodMove(1,2,cell(4,3),cell(3,2),b), 
	validgoodMove(0,4,cell(0,1),cell(1,2),r),
	validgoodMove(1,4,cell(4,3),cell(3,2),b),
	validgoodMove(1,5,cell(4,3),cell(3,2),b),
	validgoodMove(5,5,cell(3,4),cell(4,3),b),
	validgoodMove(0,6,cell(0,3),cell(1,2),r),
	validgoodMove(2,6,cell(2,1),cell(1,2),r)
}, {
	validgoodMove(0,1,cell(1,0),cell(2,1),r),
	validgoodMove(5,2,cell(3,4),cell(4,3),b),
	validgoodMove(2,2,cell(4,3),cell(3,2),b),
	validgoodMove(0,2,cell(0,2),cell(1,1),r),
	validgoodMove(2,4,cell(2,1),cell(1,2),r),
	validgoodMove(0,5,cell(0,2),cell(1,1),r),
	validgoodMove(0,6,cell(0,1),cell(1,0),r)
}, {
	move(0,cell(1,0),cell(2,1),r).move(1,cell(4,3),cell(3,2),b).move(2,cell(0,3),cell(1,2),r).move(3,cell(3,2),cell(2,3),b).move(4,cell(2,1),cell(3,2),r).move(5,cell(4,4),cell(3,3),b).
}).

%Learning Rank 3
%#pos(p1, {
%	validgoodMove(0,3,cell(0,0),cell(1,1),r)
%}, {
%	validgoodMove(2,3,cell(2,1),cell(3,2),r),
%	validgoodMove(3,3,cell(3,3),cell(4,4),b)
%}, {
%	move(0,cell(1,0),cell(2,1),r).move(1,cell(4,4),cell(3,3),b).move(2,cell(0,0),cell(1,1),r).
%}).