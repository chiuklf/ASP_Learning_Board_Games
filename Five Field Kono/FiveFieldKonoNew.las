
red(0, cell(0,0..4)).
red(0, cell(1,0)).
red(0, cell(1,4)).
blue(0, cell(4,0..4)).
blue(0, cell(3,0)).
blue(0, cell(3,4)).
empty(0, cell(1,1..3)).
empty(0, cell(2,0..4)).
empty(0, cell(3,1..3)).

turn(0,r).

turn(S,r) :- turn(S-1,b), red(S,_), not finished(S).
turn(S,b) :- turn(S-1,r), blue(S,_), not finished(S).

hasValidMove(S,r) :- red(S,cell(R1,C1)), empty(S,cell(R1-1,C1-1)), turn(S,r).
hasValidMove(S,r) :- red(S,cell(R1,C1)), empty(S,cell(R1+1,C1+1)), turn(S,r).
hasValidMove(S,r) :- red(S,cell(R1,C1)), empty(S,cell(R1+1,C1-1)), turn(S,r).
hasValidMove(S,r) :- red(S,cell(R1,C1)), empty(S,cell(R1-1,C1+1)), turn(S,r).

hasValidMove(S,b) :- blue(S,cell(R1,C1)), empty(S,cell(R1-1,C1-1)), turn(S,b).
hasValidMove(S,b) :- blue(S,cell(R1,C1)), empty(S,cell(R1+1,C1+1)), turn(S,b).
hasValidMove(S,b) :- blue(S,cell(R1,C1)), empty(S,cell(R1+1,C1-1)), turn(S,b).
hasValidMove(S,b) :- blue(S,cell(R1,C1)), empty(S,cell(R1-1,C1+1)), turn(S,b).

0 {move(S,cell(R1,C1),cell(R2,C2),P)} 1 :- validMove(S,cell(R1,C1),cell(R2,C2),P).

diagonal(cell(R1,C1),cell(R2,C2)) :- cell(R1,C1), cell(R2,C2), R2=R1+1, C2=C1+1.
diagonal(cell(R1,C1),cell(R2,C2)) :- cell(R1,C1), cell(R2,C2), R2=R1+1, C2=C1-1.
diagonal(cell(R1,C1),cell(R2,C2)) :- cell(R1,C1), cell(R2,C2), R2=R1-1, C2=C1+1.
diagonal(cell(R1,C1),cell(R2,C2)) :- cell(R1,C1), cell(R2,C2), R2=R1-1, C2=C1-1.

change(S,cell(R1,C1)) :- move(S,cell(R1,C1),_,_).
change(S,cell(R2,C2)) :- move(S,_,cell(R2,C2),_).

empty(S+1,cell(R1,C1)) :- move(S,cell(R1,C1),_,_), S<80.
empty(S+1,cell(R2,C2)) :- empty(S,cell(R2,C2)), not change(S,cell(R2,C2)), S<80.
red(S+1,cell(R2,C2)) :- move(S,cell(R1,C1),cell(R2,C2),r), S<80.
red(S+1,cell(R2,C2)) :- red(S,cell(R2,C2)), not change(S,cell(R2,C2)), S<80.
blue(S+1,cell(R2,C2)) :- move(S,cell(R1,C1),cell(R2,C2),b), S<80.
blue(S+1,cell(R2,C2)) :- blue(S,cell(R2,C2)), not change(S,cell(R2,C2)), S<80.


wins(S,b) :- blue(S,cell(0,0)),
	blue(S,cell(0,1)),
	blue(S,cell(0,2)),
	blue(S,cell(0,3)),
	blue(S,cell(0,4)),
	blue(S,cell(1,0)),
	blue(S,cell(1,4)).

wins(S,r) :- red(S,cell(3,0)),
	red(S,cell(3,4)),
	red(S,cell(4,0)),
	red(S,cell(4,1)),
	red(S,cell(4,2)),
	red(S,cell(4,3)),
	red(S,cell(4,4)).

finished(S) :- wins(S,_).

:- not finished(_).
:- move(S,cell(R1,C1),cell(R2,C2),_), move(S,cell(R3,C3),cell(R4,C4),_), R1!=R3.
:- move(S,cell(R1,C1),cell(R2,C2),_), move(S,cell(R3,C3),cell(R4,C4),_), C1!=C3.
:- move(S,cell(R1,C1),cell(R2,C2),_), move(S,cell(R3,C3),cell(R4,C4),_), R2!=R4.
:- move(S,cell(R1,C1),cell(R2,C2),_), move(S,cell(R3,C3),cell(R4,C4),_), C2!=C4.
:- hasValidMove(S,P), not move(S,_,_,P).

%validMove(S,cell(R1,C1),cell(R2,C2),P) :- turn(S,P), diagonal(cell(R1,C1),cell(R2,C2)), board(S,cell(R1,C1),P), board(S,cell(R2,C2),e).


#modeh(validMove(var(state), var(cell), var(cell), const(players))).

#modeb(1,turn(var(state), const(players))).
#modeb(1,diagonal(var(cell), var(cell))).
#modeb(1,red(var(state), var(cell))).
#modeb(1,blue(var(state), var(cell))).
#modeb(1,empty(var(state),var(cell))).

#constant(player, r).
#constant(player, b).

#maxv(4).
#max_penalty(20).

#pos(p0, {validMove(0,cell(0,0),cell(1,1),r),validMove(0,cell(0,1),cell(1,2),r),validMove(0,cell(0,2),cell(1,1),r),validMove(0,cell(0,2),cell(1,3),r),validMove(0,cell(0,3),cell(1,2),r),validMove(0,cell(0,4),cell(1,3),r),validMove(0,cell(1,0),cell(2,1),r),validMove(0,cell(1,4),cell(2,3),r),validMove(1,cell(3,4),cell(2,3),b),validMove(1,cell(4,0),cell(3,1),b),validMove(1,cell(4,1),cell(3,2),b),validMove(1,cell(4,2),cell(3,1),b),validMove(1,cell(4,2),cell(3,3),b),validMove(1,cell(4,3),cell(3,2),b),validMove(1,cell(4,4),cell(3,3),b)},{validMove(0,cell(1,1),cell(2,2),r),validMove(1,cell(3,0),cell(2,1),b),validMove(0,cell(3,0),cell(2,1),b),validMove(0,cell(0,0),cell(2,2),r)},{red(0,cell(0,0)).red(0,cell(0,1)).red(0,cell(0,2)).red(0,cell(0,3)).red(0,cell(0,4)).red(0,cell(1,0)).empty(0,cell(1,1)).empty(0,cell(1,2)).empty(0,cell(1,3)).red(0,cell(1,4)).empty(0,cell(2,0)).empty(0,cell(2,1)).empty(0,cell(2,2)).empty(0,cell(2,3)).empty(0,cell(2,4)).blue(0,cell(3,0)).empty(0,cell(3,1)).empty(0,cell(3,2)).empty(0,cell(3,3)).blue(0,cell(3,4)).blue(0,cell(4,0)).blue(0,cell(4,1)).blue(0,cell(4,2)).blue(0,cell(4,3)).blue(0,cell(4,4)).red(1,cell(0,0)).red(1,cell(0,1)).red(1,cell(0,2)).red(1,cell(0,3)).red(1,cell(0,4)).empty(1,cell(1,0)).empty(1,cell(1,1)).empty(1,cell(1,2)).empty(1,cell(1,3)).red(1,cell(1,4)).empty(1,cell(2,0)).red(1,cell(2,1)).empty(1,cell(2,2)).empty(1,cell(2,3)).empty(1,cell(2,4)).blue(1,cell(3,0)).empty(1,cell(3,1)).empty(1,cell(3,2)).empty(1,cell(3,3)).blue(1,cell(3,4)).blue(1,cell(4,0)).blue(1,cell(4,1)).blue(1,cell(4,2)).blue(1,cell(4,3)).blue(1,cell(4,4)).turn(1,b).turn(0,r).}).

%#pos(p0, {validMove(0,cell(0,0),cell(1,1),r),validMove(0,cell(0,1),cell(1,2),r),validMove(0,cell(0,2),cell(1,1),r),validMove(0,cell(0,2),cell(1,3),r),validMove(0,cell(0,3),cell(1,2),r),validMove(0,cell(0,4),cell(1,3),r),validMove(0,cell(1,0),cell(2,1),r),validMove(0,cell(1,4),cell(2,3),r),validMove(1,cell(3,4),cell(2,3),b),validMove(1,cell(4,0),cell(3,1),b),validMove(1,cell(4,1),cell(3,2),b),validMove(1,cell(4,2),cell(3,1),b),validMove(1,cell(4,2),cell(3,3),b),validMove(1,cell(4,3),cell(3,2),b),validMove(1,cell(4,4),cell(3,3),b)},{validMove(0,cell(4,2),cell(3,3),b),validMove(0,cell(0,1),cell(1,0),r),validMove(0,cell(1,1),cell(2,2),r)},{board(0,cell(0,0),r).board(0,cell(0,1),r).board(0,cell(0,2),r).board(0,cell(0,3),r).board(0,cell(0,4),r).board(0,cell(1,0),r).board(0,cell(1,1),e).board(0,cell(1,2),e).board(0,cell(1,3),e).board(0,cell(1,4),r).board(0,cell(2,0),e).board(0,cell(2,1),e).board(0,cell(2,2),e).board(0,cell(2,3),e).board(0,cell(2,4),e).board(0,cell(3,0),b).board(0,cell(3,1),e).board(0,cell(3,2),e).board(0,cell(3,3),e).board(0,cell(3,4),b).board(0,cell(4,0),b).board(0,cell(4,1),b).board(0,cell(4,2),b).board(0,cell(4,3),b).board(0,cell(4,4),b).board(1,cell(0,0),r).board(1,cell(0,1),r).board(1,cell(0,2),r).board(1,cell(0,3),r).board(1,cell(0,4),r).board(1,cell(1,0),e).board(1,cell(1,1),e).board(1,cell(1,2),e).board(1,cell(1,3),e).board(1,cell(1,4),r).board(1,cell(2,0),e).board(1,cell(2,1),r).board(1,cell(2,2),e).board(1,cell(2,3),e).board(1,cell(2,4),e).board(1,cell(3,0),b).board(1,cell(3,1),e).board(1,cell(3,2),e).board(1,cell(3,3),e).board(1,cell(3,4),b).board(1,cell(4,0),b).board(1,cell(4,1),b).board(1,cell(4,2),b).board(1,cell(4,3),b).board(1,cell(4,4),b).turn(1,b).turn(0,r).}).
