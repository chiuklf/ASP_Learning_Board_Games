% Individual Project ASP
%
% 2 Players, 7 counters each (say red (R), blue(B)) 
% placed on either side of a 25 point grid (E = empty)
%
%	R R R R R
%	R E E E R
%	E E E E E
%	B E E E B
%	B B B B B
%
% Objective is to get all counters on the opposite side.
%
% Moves:
% Players in turn move diagonally 1 row/column at a time
% If the points are numbered 1..5, 6..10 etc, 
% then players can move 7 - 1, 7 - 3, 7 -11, 7 - 13, etc. 
% as long as the point is vacant.

% Initial Board state
% Top Left = (0, 0), Bottom Right = (4, 4)
% board(state, row, column, colour)
cell(0..2,0..2).

board(0,cell(0,0),e).
board(0,cell(0,1),r).
board(0,cell(0,2),e).

board(0,cell(1,0),e).
board(0,cell(1,1),e).
board(0,cell(1,2),e).

board(0,cell(2,0),e).
board(0,cell(2,1),e).
board(0,cell(2,2),b).

turn(0,r).

% Two players
player(r).
player(b).

% State
%#const max=100.

turn(S,r) :- turn(S-1,b), board(S,_,_), not finished(S).
turn(S,b) :- turn(S-1,r), board(S,_,_), not finished(S).

% Valid Move
% validMove(state, row1, col1, row2, col2, player)
%validMove(S,cell(R1,C1),cell(R2,C2),P) :- turn(S,P), player(P), diagonal(cell(R1,C1),cell(R2,C2)), toEmptyBoard(S,cell(R1,C1),cell(R2,C2),P).

%toEmptyBoard(S,cell(R1,C1),cell(R2,C2),P) :- board(S,cell(R1,C1),P), board(S,cell(R2,C2),e).

%hasValidMove(S,P) :- board(S,cell(R1,C1),P), board(S,cell(R1-1,C1-1),e), turn(S,P).
%hasValidMove(S,P) :- board(S,cell(R1,C1),P), board(S,cell(R1+1,C1+1),e), turn(S,P).
%hasValidMove(S,P) :- board(S,cell(R1,C1),P), board(S,cell(R1+1,C1-1),e), turn(S,P).
%hasValidMove(S,P) :- board(S,cell(R1,C1),P), board(S,cell(R1-1,C1+1),e), turn(S,P).

%0 {move(S,cell(R1,C1),cell(R2,C2),P)} 1 :- validMove(S,cell(R1,C1),cell(R2,C2),P).

diagonal(cell(R1,C1),cell(R2,C2)) :- board(_,cell(R1,C1),_), board(_,cell(R2,C2),_), R2=R1+1, C2=C1+1.
diagonal(cell(R1,C1),cell(R2,C2)) :- board(_,cell(R1,C1),_), board(_,cell(R2,C2),_), R2=R1+1, C2=C1-1.
diagonal(cell(R1,C1),cell(R2,C2)) :- board(_,cell(R1,C1),_), board(_,cell(R2,C2),_), R2=R1-1, C2=C1+1.
diagonal(cell(R1,C1),cell(R2,C2)) :- board(_,cell(R1,C1),_), board(_,cell(R2,C2),_), R2=R1-1, C2=C1-1.

%change(S,cell(R1,C1)) :- move(S,cell(R1,C1),_,P).
%change(S,cell(R2,C2)) :- move(S,_,cell(R2,C2),P).

%board(S+1,cell(R1,C1),e) :- move(S,cell(R1,C1),_,P), player(P), S<max.
%board(S+1,cell(R2,C2),P) :- move(S,_,cell(R2,C2),P), player(P), S<max.
%board(S+1,cell(R2,C2),P) :- board(S,cell(R2,C2),P), not change(S,cell(R2,C2)), S<max.

finished(S) :- board(S,cell(0,0),b).

finished(S) :- board(S,cell(2,2),r).

%:- not finished(_).
%:- move(S,cell(R1,C1),cell(R2,C2),P), move(S,cell(R3,C3),cell(R4,C4),P), R1!=R3.
%:- move(S,cell(R1,C1),cell(R2,C2),P), move(S,cell(R3,C3),cell(R4,C4),P), C1!=C3.
%:- move(S,cell(R1,C1),cell(R2,C2),P), move(S,cell(R3,C3),cell(R4,C4),P), R2!=R4.
%:- move(S,cell(R1,C1),cell(R2,C2),P), move(S,cell(R3,C3),cell(R4,C4),P), C2!=C4.
%:- hasValidMove(S,P), not move(S,_,_,P).

#modeh(validMove(var(state), var(cell1), var(cell2), var(players))).
%#modeh(toEmptyBoard(var(state), var(cell1), var(cell2), var(players))).
#modeb(1,turn(var(state), var(players))).
%#modeb(1,player(var(players))).
#modeb(1,diagonal(var(cell1), var(cell2))).
#modeb(1,board(var(state), var(cell1), var(players))).
#modeb(1,board(var(state), var(cell2), const(piece))).
%#modeb(1,toEmptyBoard(var(state), var(cell1), var(cell2), var(players))).

#constant(piece, e).

#maxv(5).
#max_penalty(10).

%Gnerated Examples

%#pos(p0, {board(1,cell(0,0),e),board(1,cell(0,1),e),board(1,cell(0,2),e),board(1,cell(1,0),e),board(1,cell(1,1),r),board(1,cell(1,2),e),board(1,cell(2,0),e),board(1,cell(2,1),e),board(1,cell(2,2),b)},{},{board(0,cell(0,0),r).board(0,cell(0,1),e).board(0,cell(0,2),e).board(0,cell(1,0),e).board(0,cell(1,1),e).board(0,cell(1,2),e).board(0,cell(2,0),e).board(0,cell(2,1),e).board(0,cell(2,2),b).}).

#pos(p0, {validMove(0,cell(0,1),cell(1,0),r),validMove(0,cell(0,1),cell(1,2),r)},{validMove(0,cell(0,1),cell(1,1),r),validMove(0,cell(2,2),cell(1,1),b)},{board(0,cell(0,0),e).board(0,cell(0,1),r).board(0,cell(0,2),e).board(0,cell(1,0),e).board(0,cell(1,1),e).board(0,cell(1,2),e).board(0,cell(2,0),e).board(0,cell(2,1),e).board(0,cell(2,2),b).}).

#pos(p1, {validMove(0,cell(0,1),cell(1,0),r),validMove(0,cell(0,1),cell(1,2),r)},{validMove(0,cell(0,1),cell(1,1),r),validMove(0,cell(2,2),cell(1,1),b)},{board(0,cell(0,0),e).board(0,cell(0,1),r).board(0,cell(0,2),e).board(0,cell(1,0),e).board(0,cell(1,1),e).board(0,cell(1,2),e).board(0,cell(2,0),e).board(0,cell(2,1),e).board(0,cell(2,2),b).}).
