def initBoard()
  board = Array.new(5){Array.new(5)}
  for i in 0..4
    board[0][i] = 'r'
    board[4][i] = 'b'
  end
  board[1][0] = 'r'
  board[1][4] = 'r'
  board[3][0] = 'b'
  board[3][4] = 'b'
  board[2][0] = 'e'
  board[2][4] = 'e'
  for i in 1..3
    for j in 1..3
      board[i][j] = 'e'
    end
  end
  return board
end

def generateBoard(board, turn)
  result = String.new
  for i in 0..4
    for j in 0..4
      result += "board(#{turn},cell(#{i},#{j}),#{board[i][j]})."
    end
  end
  return result
end

def generateValidMove(board, turn)
  result = String.new
  for i in 0..4
    for j in 0..4
      if board[i][j] != 'r' && board[i][j] != 'b'
        next
      end
      if i-1 >= 0 && j-1 >= 0 && board[i-1][j-1] == 'e'
        result += "validMove(#{turn},cell(#{i},#{j}),cell(#{i-1},#{j-1}),#{board[i][j]}),"
      end
      if i-1 >= 0 && j+1 <= 4 && board[i-1][j+1] == 'e'
        result += "validMove(#{turn},cell(#{i},#{j}),cell(#{i-1},#{j+1}),#{board[i][j]}),"
      end
      if i+1 <= 4 && j-1 >= 0 && board[i+1][j-1] == 'e'
        result += "validMove(#{turn},cell(#{i},#{j}),cell(#{i+1},#{j-1}),#{board[i][j]}),"
      end
      if i+1 <= 4 && j+1 <= 4 && board[i+1][j+1] == 'e'
        result += "validMove(#{turn},cell(#{i},#{j}),cell(#{i+1},#{j+1}),#{board[i][j]}),"
      end
    end
  end
  result[-1] = '.'
  return result
end

def move(board, player)
  while true
    i = rand(0..4)
    j = rand(0..4)
    if board[i][j] == 'e' || board[i][j] != player
      next
    end
    possibleMoves = Array.new
    if i-1 >= 0 && j-1 >= 0 && board[i-1][j-1] == 'e'
      possibleMoves.push(1)
    end
    if i-1 >= 0 && j+1 <= 4 && board[i-1][j+1] == 'e'
      possibleMoves.push(2)
    end
    if i+1 <= 4 && j-1 >= 0 && board[i+1][j-1] == 'e'
      possibleMoves.push(3)
    end
    if i+1 <= 4 && j+1 <= 4 && board[i+1][j+1] == 'e'
      possibleMoves.push(4)
    end

    if possibleMoves.empty?
      next
    end

    newBoard = board
    case possibleMoves.sample
    when 1
      newBoard[i-1][j-1] = board[i][j]
      newBoard[i][j] = 'e'
      puts "<1>: #{newBoard}"
      return newBoard
    when 2
      newBoard[i-1][j+1] = board[i][j]
      newBoard[i][j] = 'e'
      puts "<2>: #{newBoard}"
      return newBoard
    when 3
      newBoard[i+1][j-1] = board[i][j]
      newBoard[i][j] = 'e'
      puts "<3>: #{newBoard}"
      return newBoard
    when 4
      newBoard[i+1][j+1] = board[i][j]
      newBoard[i][j] = 'e'
      puts "<4>: #{newBoard}"
      return newBoard
    end 
  end
end

numberOfEg = 1
def generatePos()
  board = initBoard()
  boards = String.new
  moves = String.new
  numOfTurn = 2
  for i in 0..numOfTurn
    boards += generateBoard(board, i)
    moves += generateValidMove(board, i)
    if i%2 == 0
      board = move(board, 'r')
    else
      board = move(board, 'b')
    end
  end
  return boards, moves
end

result = "\n\%Gnerated Examples\n"

for i in 0..numberOfEg-1
  boards, moves = generatePos()
  result += "#pos(p#{i}, {#{moves}},{},{#{boards}})."
  result += "\n"
end

open('test.las', 'a') {|f|
  f.puts result
}
